﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SfuPsi_DomainClasses
{
    public class Level
    {
        public int LevelId { get; set; }

        [Required]
        public string LevelTitle { get; set; }

        public virtual ICollection<Person> Students { get; set; }
    }
}