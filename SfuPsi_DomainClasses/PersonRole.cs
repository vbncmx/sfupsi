﻿using System;

namespace SfuPsi_DomainClasses
{
    public class PersonRole
    {
        public int PersonRoleId { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}