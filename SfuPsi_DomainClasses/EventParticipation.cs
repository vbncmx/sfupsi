﻿namespace SfuPsi_DomainClasses
{
    public class EventParticipation
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int PersonId { get; set; }
        public string Note { get; set; }
    }
}