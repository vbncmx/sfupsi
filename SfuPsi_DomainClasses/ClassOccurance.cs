﻿using System;

namespace SfuPsi_DomainClasses
{
    public class ClassOccurance
    {
        public int ClassOccuranceId { get; set; }
        public int ClassId { get; set; }
        public virtual Class Class { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan Duration { get; set; }
    }
}