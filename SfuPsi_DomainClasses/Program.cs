﻿using System.ComponentModel.DataAnnotations;

namespace SfuPsi_DomainClasses
{
    public class Program
    {
        public int ProgramId { get; set; }

        [Required]
        public string Title { get; set; }
    }
}