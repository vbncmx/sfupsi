﻿namespace SfuPsi_DomainClasses
{
    public class Role
    {
        public const int ProfessorRoleId = 101, StudentRoleId = 102;
        public const string ProfessorRoleTitle = "Professor", StudentRoleTitle = "Student";

        public static int[] BuiltInRoleIds => new[] { ProfessorRoleId, StudentRoleId};

        public int RoleId { get; set; }
        public string RoleTitle { get; set; }

        public int Order { get; set; }
    }
}