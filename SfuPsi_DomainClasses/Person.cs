﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SfuPsi_DomainClasses
{
    public class Person
    {
        public int PersonId { get; set; }

        // Profile
        [Required]
        public string Name { get; set; }




        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public bool IsPhotoLoaded { get; set; }
        public string Languages { get; set; }

        // Contacts
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Street { get; set; }
        public string WebSite { get; set; }

        // Student Data
        public DateTime? StudentAdmissionDate { get; set; }
        public string StudentAnalyst { get; set; }
        public string StudentSupervision { get; set; }
        public string StudentPreExam { get; set; }
        public string StudentK1 { get; set; }
        public DateTime? StudentK1Date { get; set; }
        public string StudentK2 { get; set; }
        public DateTime? StudentK2Date { get; set; }

        public int? CurrentLevelId { get; set; }
        [ForeignKey(nameof(CurrentLevelId))]
        public Level CurrentLevel { get; set; }

        public int? ProgramId { get; set; }
        [ForeignKey(nameof(ProgramId))]
        public Program Program { get; set; }

        public virtual ICollection<Class> TeachedClasses { get; set; }

        public virtual ICollection<PersonRole> PersonRoles { get; set; }


        public void SetName(string name)
        {
            Name = name.TrimStart(' ').TrimEnd(' ');
        }
    }
}