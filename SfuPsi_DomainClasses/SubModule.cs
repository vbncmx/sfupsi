﻿namespace SfuPsi_DomainClasses
{
    public class SubModule
    {
        public int SubModuleId { get; set; }
        public int ModuleId { get; set; }
        public virtual Module Module { get; set; }
        public string Title { get; set; }
    }
}