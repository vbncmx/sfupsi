﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SfuPsi_DomainClasses
{
    public class ClassParticipation
    {
        public int Id { get; set; }

        public int ClassTemplateId { get; set; }
        [ForeignKey(nameof(ClassTemplateId))]
        public virtual ClassTemplate ClassTemplate { get; set; }

        public int PersonId { get; set; }
        [ForeignKey(nameof(PersonId))]
        public virtual Person Person { get; set; }

        public string Note { get; set; }

        public int? SemesterId { get; set; }
        [ForeignKey(nameof(SemesterId))]
        public virtual Semester Semester { get; set; }
    }
}