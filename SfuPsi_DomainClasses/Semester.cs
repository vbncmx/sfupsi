﻿using System.ComponentModel.DataAnnotations;

namespace SfuPsi_DomainClasses
{
    public class Semester
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; } 
    }
}