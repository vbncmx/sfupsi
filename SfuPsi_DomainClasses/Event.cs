﻿using System;

namespace SfuPsi_DomainClasses
{
    public class Event
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime? Date { get; set; }
        public string Note { get; set; }
    }
}