﻿using System.Collections.Generic;

namespace SfuPsi_DomainClasses
{
    public class Class
    {
        public int ClassId { get; set; }
        public int LevelId { get; set; }
        public virtual Level Level { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Methodology { get; set; }
        public string SuccessCondition { get; set; }
        public string Links { get; set; }
        public string RecommendedLiterature { get; set; }
        public string ProfessorComment { get; set; }
        public int? ClassTemplateId { get; set; }
        public virtual ClassTemplate ClassTemplate { get; set; }
        public int? SubModuleId { get; set; }
        public virtual SubModule SubModule { get; set; }
        public virtual ICollection<Person> Professors { get; set; }
    }
}