﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SfuPsi_DomainClasses
{
    public class ClassTemplate
    {
        public int ClassTemplateId { get; set; }

        [Required]
        public string ClassTemplateTitle { get; set; }

        public int LevelId { get; set; }
        public virtual Level Level { get; set; }
        
        public string ClassTitle { get; set; }
        public string ClassDescription { get; set; }
        public string ClassMethodology { get; set; }
        public string ClassSuccessCondition { get; set; }
        public string ClassLinks { get; set; }
        public string ClassRecommendedLiterature { get; set; }
        public string ClassProfessorComment { get; set; }

        [ForeignKey(nameof(ClassSubModuleId))]
        public virtual SubModule ClassSubModule { get; set; }
        public int? ClassSubModuleId { get; set; }

        public int? ProgramId { get; set; }
        [ForeignKey(nameof(ProgramId))]
        public virtual Program Program { get; set; }
    }
}