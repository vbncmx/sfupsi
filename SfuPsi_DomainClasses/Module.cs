﻿using System.Collections.Generic;

namespace SfuPsi_DomainClasses
{
    public class Module
    {
        public int ModuleId { get; set; }
        public string ModuleTitle { get; set; }
        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }

        public virtual ICollection<SubModule>  SubModules { get; set; }
    }
}