﻿using System;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application
{
    public static class DomainClassExtensions
    {
        public static string PhotoUrl(this Person person)
        {
            return person.IsPhotoLoaded
                ? $"/Images/Uploads/PersonPhoto_{person.PersonId}.jpg?{DateTime.Now}"
                : "/Images/nophoto.jpg";
        }
    }
}