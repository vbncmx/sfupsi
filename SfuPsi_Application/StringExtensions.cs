﻿using System;
using System.Globalization;

namespace SfuPsi_Application
{
    public static class StringExtensions
    {
        public static DateTime? ParseAsDate(this string dateString)
        {
            DateTime parsedDate;
            var parseDateResult = DateTime.TryParseExact(dateString, Formats.CommonDateFormat,
                CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate);
            return parseDateResult ? (DateTime?)parsedDate : null;
        }
    }
}