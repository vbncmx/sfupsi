﻿using System;

namespace SfuPsi_Application.DateTimeService
{
    public static class DtServiceExtensions
    {
        public static DateTime Today(this DtService dtService)
        {
            return dtService.Now().Date;
        }
    }
}