﻿using System;

namespace SfuPsi_Application.DateTimeService
{
    public class UtcDtService : DtService
    {
        private readonly int _utc;

        public UtcDtService(int utc)
        {
            _utc = utc;
        }

        public override DateTime Now()
        {
            return DateTime.UtcNow.AddHours(_utc);
        }
    }
}