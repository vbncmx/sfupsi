﻿using System;
using System.Configuration;

namespace SfuPsi_Application.DateTimeService
{
    public abstract class DtService
    {
        private static DtService _current;
        public static DtService Current
        {
            get
            {
                if (_current == null)
                {
                    var utc = int.Parse(ConfigurationManager.AppSettings["UTC"]);
                    _current = new UtcDtService(utc);
                }
                return _current;
            }
            set { throw new NotImplementedException(); }
        }

        public abstract DateTime Now();
    }
}