﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using SfuPsi_Application.Identity;

namespace SfuPsi_Application
{
    public static class ControllerExtensions
    {
        public static AppUserManager UserManager(this Controller controller)
        {
            return controller.HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
        }
    }
}