﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Identity;
using SfuPsi_DomainData;
using System.Web;

namespace SfuPsi_Application
{
    public class MvcApplication : HttpApplication
    {
        private static UnityContainer _container;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SfuPsiContext.Configure();

            IdentityContext.Configure();

            StartUnity();
        }

        protected void Application_Shutdown()
        {
            StopUnity();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Add(Constants.SessionActivityLogIdKey, Guid.NewGuid().ToString());
        }

        private void StartUnity()
        {
            _container = new UnityContainer();

            var actionLoggingType = ConfigurationManager.AppSettings["ActionLogging"].ToLower();
            if (actionLoggingType == "db")
                _container.RegisterType<ILogActionContext, DbActionLogger>();
            else
                _container.RegisterType<ILogActionContext, NoneActionLogger>();

            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new ActionLog.UnityFilterAttributeFilterProvider(_container));

            DependencyResolver.SetResolver(new UnityDependencyResolver(_container));
        }

        private void StopUnity()
        {
            _container?.Dispose();
        }

        //public void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    var threadCultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
        //    var uiCultureInfo = (CultureInfo)CultureInfo.CurrentUICulture.Clone();
        //    threadCultureInfo.DateTimeFormat.ShortDatePattern =
        //          ReformatDateTime(threadCultureInfo.DateTimeFormat.ShortDatePattern);
        //    uiCultureInfo.DateTimeFormat.ShortDatePattern =
        //          ReformatDateTimeUi(uiCultureInfo.DateTimeFormat.ShortDatePattern);
        //    Thread.CurrentThread.CurrentCulture = threadCultureInfo;
        //    Thread.CurrentThread.CurrentUICulture = uiCultureInfo;
        //}

        //private static string ReformatDateTime(string format)
        //{
        //    return format;
        //}

        //private static string ReformatDateTimeUi(string format)
        //{
        //    return format;
        //}
    }
}
