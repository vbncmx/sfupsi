namespace SfuPsi_Application.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_UserName_To_ActionLogRecord : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ActionLogRecords", "UserName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ActionLogRecords", "UserName");
        }
    }
}
