namespace SfuPsi_Application.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ActionLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionLogRecords",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(maxLength: 128),
                        SessionId = c.String(),
                        Controller = c.String(),
                        Action = c.String(),
                        Parameters = c.String(),
                        DateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActionLogRecords", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.ActionLogRecords", new[] { "UserId" });
            DropTable("dbo.ActionLogRecords");
        }
    }
}
