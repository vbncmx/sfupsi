namespace SfuPsi_Application.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Limit_Events_Table_To_500_Records : DbMigration
    {
        public override void Up()
        {
            var query =
                "IF EXISTS(SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[LimitActionLogTable]'))\n" +
                "DROP TRIGGER[dbo].[LimitActionLogTable]\n" +
                "GO\n" +
                "CREATE TRIGGER LimitActionLogTable ON ActionLogRecords AFTER INSERT\n" +
                "AS\n" +
                "DECLARE @tableCount int\n" +
                "SELECT @tableCount = COUNT(*) FROM ActionLogRecords\n" +
                "IF @tableCount > 500\n" +
                "BEGIN\n" +
                "DELETE FROM ActionLogRecords WHERE Id IN(SELECT TOP (@tableCount - 500) Id FROM ActionLogRecords ORDER BY DateTime)\n" +
                "END\n" +
                "GO";
            Sql(query);
        }
        
        public override void Down()
        {
        }
    }
}
