﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;


namespace SfuPsi_Application
{
    public static class DtHelper
    {
        public static void FixModelDateTimeFields<TModel>(TModel model, ModelStateDictionary modelStateDictionary)
        {
            // not nullable
            var fields = typeof (TModel).GetProperties()
                .Where(p => p.PropertyType == typeof (DateTime)).ToArray();
            foreach (var property in fields)
            {
                var @value = Parse(modelStateDictionary[property.Name].Value.AttemptedValue);
                property.SetValue(model, @value);
            }

            // nullable
            var nullableFields = typeof (TModel).GetProperties()
                .Where(p => p.PropertyType.IsGenericType &&
                p.PropertyType.GetGenericTypeDefinition() == typeof (Nullable<>) &&
                p.PropertyType.GetGenericArguments()[0] == typeof (DateTime))
                .ToArray();
            foreach (var property in nullableFields)
            {
                var @value = ParseNullable(modelStateDictionary[property.Name].Value.AttemptedValue);
                property.SetValue(model, @value);
            }
        }

        public static DateTime Parse(string dateTimeString)
        {
            if (string.IsNullOrWhiteSpace(dateTimeString))
                return DateTime.MinValue;
            string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                         "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                         "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                         "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                         "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};
            return DateTime.ParseExact(dateTimeString, formats,
                    new CultureInfo("en-US"), DateTimeStyles.None);
        }

        public static DateTime? ParseNullable(string dateTimeString)
        {
            if (string.IsNullOrWhiteSpace(dateTimeString))
                return null;
            return Parse(dateTimeString);
        }
    }
}