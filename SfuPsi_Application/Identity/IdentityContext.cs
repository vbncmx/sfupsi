﻿using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SfuPsi_Application.Migrations;

namespace SfuPsi_Application.Identity
{
    public class IdentityContext : IdentityDbContext<AppUser>
    {
        public IdentityContext() : base("IdentityCs") { }

        public DbSet<ActionLogRecord> ActionLogRecords { get; set; }

        public static void Configure()
        {
            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<IdentityContext, Configuration>());

            var identityContext = new IdentityContext();

            // verify that admin role is in DB
            if (!identityContext.Roles.Any(r => r.Name == AppRole.AdminRoleName))
            {
                var store = new RoleStore<AppRole>(identityContext);
                var manager = new RoleManager<AppRole>(store);
                var role = new AppRole { Name = AppRole.AdminRoleName, Id = AppRole.AdminRoleId };
                manager.Create(role);
            }

            // verify that admin user is in DB
            if (!identityContext.Users.Any(u => u.UserName == AppUser.AdminUsername))
            {
                var store = new UserStore<AppUser>(identityContext);
                var manager = new UserManager<AppUser>(store);
                var user = new AppUser { UserName = AppUser.AdminUsername, Id = AppUser.AdminId };

                manager.Create(user, AppUser.AdminDefaultPassword);
                manager.AddToRole(user.Id, AppRole.AdminRoleName);
            }
        }
    }
}