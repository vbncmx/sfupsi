﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace SfuPsi_Application.Identity
{
    public class AppUser : IdentityUser
    {
        public const int MinPasswordLength = 5;
        public const string AdminId = "86C817AF-C545-4816-B7AE-46A877CBF080", AdminUsername = "admin", AdminDefaultPassword = "iamadmin";
    }
}