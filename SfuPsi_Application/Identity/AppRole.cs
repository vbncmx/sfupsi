﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace SfuPsi_Application.Identity
{
    public class AppRole : IdentityRole
    {
        public const string AdminRoleId = "1A890A4E-80F5-47FB-9169-7321EE9DA14E",
            AdminRoleName = "admin";
        public AppRole() : base() { }
        public AppRole(string name) : base(name) { }
    }
}