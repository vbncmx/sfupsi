﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.AspNet.Identity;
using Constants = SfuPsi_Application.ActionLog.Constants;

namespace SfuPsi_Application.Identity
{
    public class ActionLogRecord
    {
        public string Id { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual AppUser User { get; set; }

        public string UserName { get; set; }

        public string SessionId { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Parameters { get; set; }

        public DateTime DateTime { get; set; }
    }

    public static class ActionExecutingContextExtensions
    {
        public static ActionLogRecord ToActionLogRecord(this ActionExecutingContext actionContext)
        {
            var parameters = actionContext.ActionParameters;
            var serializer = new JavaScriptSerializer();
            return new ActionLogRecord
            {
                Id = Guid.NewGuid().ToString(),
                UserId = actionContext.HttpContext.User.Identity.GetUserId(),
                UserName = actionContext.HttpContext.User.Identity.Name,
                SessionId = actionContext.HttpContext.Session[Constants.SessionActivityLogIdKey].ToString(),
                Controller = actionContext.Controller.GetType().Name,
                Action = actionContext.ActionDescriptor.ActionName,
                DateTime = DateTime.Now,
                Parameters = parameters == null || parameters.Count == 0 ? null : serializer.Serialize(parameters)
            };
        }
    }
}