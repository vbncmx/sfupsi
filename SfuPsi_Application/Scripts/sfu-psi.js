﻿function formatDateElement(args) {
    console.dir(args);
    var element = args.element;
    element.kendoDatePicker({
        format: "dd.MM.yyyy"
    });
}

function displayAjaxError(xhr, status, p3, p4) {
    var err = "Error " + " " + status + " " + p3 + " " + p4;
    if (xhr.responseText && xhr.responseText[0] == "{")
        err = JSON.parse(xhr.responseText).Message;
    console.log(err);
    alert(err);
}

function LoadGridLayout(gridId) {
    var gridSelector = "#" + gridId.replace("#", "");
    var grid = $(gridSelector).data("kendoGrid");
    var localStorageKey = "grid-layout-" + gridId.replace("#", "").toLowerCase();
    var optionsString = localStorage[localStorageKey];
    if (optionsString) {
        var options = JSON.parse(optionsString);
        grid.setOptions(options);
    }
}

function SaveGridLayout(e) {
    var gridId = e.sender.element[0].id;
    var grid = $("#" + gridId).data("kendoGrid");
    if (grid == undefined) return;
    var options = grid.getOptions();
    var optionsString = kendo.stringify(options);
    var localStorageKey = "grid-layout-" + gridId.toLowerCase();
    localStorage[localStorageKey] = optionsString;
}

function AddGridRow(gridId) {
    var gridSelector = "#" + gridId.replace("#", "");
    var grid = $(gridSelector).data("kendoGrid");
    grid.addRow();
}

function GridErrorHandlerAlert(e) {
    if (e.errors) {
        e.preventDefault(); // cancel grid rebind if error occurs
        var message = "Errors:\n";
        $.each(e.errors, function (key, value) {
            if ('errors' in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
        alert(message);
        e.cancelChanges();
    }
}

function ConfigureTimeEditor() {
    kendo.data.binders.widget.timeVm = kendo.data.Binder.extend({
        init: function (widget, bindings, options) {
            kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
            this.widget = widget;
            this._change = $.proxy(this.change, this);
            this.widget.bind("change", this._change);
        },
        refresh: function () {
            var value = this.bindings.timeVm.get();
            var date = null;
            if (value) {
                date = new Date();
                date.setHours(value.Hours);
                date.setMinutes(value.Minutes);
            }

            this.widget.value(date);
        },
        change: function () {
            var date = this.widget.value();
            var value = null;
            if (date) {
                value = {
                    Hours: date.getHours(),
                    Minutes: date.getMinutes()
                };
            }
            this.bindings.timeVm.set(value);
        },
        destroy: function () {
            this.widget.unbind("change", this._change);
        }
    });
}

function ToDateTime(secs) {
    var t = new Date(1970, 0, 1);
    t.setSeconds(secs);
    return t;
}

function ExpandRows(gridId, rowPredicate) {
    var gridSelector = "#" + gridId.replace("#", "");
    var grid = $(gridSelector).data("kendoGrid");
    var data = grid.dataSource.data();
    var rowCount = data.length;
    for (var i = 0; i < rowCount; i++) {
        var row = data[i];
        if (rowPredicate(row)) {
            grid.expandRow("tr[data-uid='" + row.uid + "']");
        }
    }
}

function CopyEmails(gridId, inputId, tickId, fieldFunc) {

    var gridSelector = "#" + gridId.replace("#", "");
    var inputSelector = "#" + inputId.replace("#", "");
    var tickSelector = "#" + tickId.replace("#", "");

    // Gets the data source from the grid.
    var dataSource = $(gridSelector).data("kendoGrid").dataSource;

    // Gets the filter from the dataSource
    var filters = dataSource.filter();

    // Gets the full set of data from the data source
    var allData = dataSource.data();

    // Applies the filter to the data
    var query = new kendo.data.Query(allData);
    var filteredData = query.filter(filters).data;

    // Construct email line & feedback message
    var emailLine = "";
    $.each(filteredData, function (index, item) {
        emailLine += fieldFunc(item) + ";";
    });

    var emailInput = $(inputSelector);
    emailInput.val(emailLine);
    emailInput.select();
    document.execCommand("copy");

    var tick = $(tickSelector);
    tick.show(400, function () {
        tick.hide(400);
    });
}

