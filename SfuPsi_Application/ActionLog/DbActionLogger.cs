﻿using System;
using System.Web.Mvc;
using SfuPsi_Application.Identity;

namespace SfuPsi_Application.ActionLog
{
    public class DbActionLogger : ILogActionContext
    {
        private readonly IdentityContext _context;

        public DbActionLogger(IdentityContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            _context = context;
        }

        public void LogActionContext(ActionExecutingContext actionContext)
        {
            var actionLogRecord = actionContext.ToActionLogRecord();
            _context.ActionLogRecords.Add(actionLogRecord);
            _context.SaveChangesAsync();
        }
    }
}