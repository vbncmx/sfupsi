﻿using System.Web.Mvc;

namespace SfuPsi_Application.ActionLog
{
    internal class NoneActionLogger : ILogActionContext
    {
        public void LogActionContext(ActionExecutingContext actionContext) { }
    }
}