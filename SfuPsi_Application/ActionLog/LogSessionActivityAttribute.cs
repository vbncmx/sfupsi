﻿using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;

namespace SfuPsi_Application.ActionLog
{
    public class LogSessionActivityAttribute : ActionFilterAttribute
    {
        [Dependency]
        public ILogActionContext Logger { get; set; }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (actionContext.HttpContext.Session?[Constants.SessionActivityLogIdKey] != null)
            {
                Logger.LogActionContext(actionContext);
            }
            base.OnActionExecuting(actionContext);
        }
    }
}