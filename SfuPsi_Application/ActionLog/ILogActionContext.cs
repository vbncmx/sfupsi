﻿using System.Web.Mvc;

namespace SfuPsi_Application.ActionLog
{
    public interface ILogActionContext
    {
        void LogActionContext(ActionExecutingContext actionContext);
    }
}