using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SfuPsi_Application.ActionLog
{
    public class DebugActionLogger : ILogActionContext
    {
        public void LogActionContext(ActionExecutingContext actionContext)
        {
            var parameters = actionContext.ActionParameters;
            var serializer = new JavaScriptSerializer();
            var parameterString = parameters == null || parameters.Count == 0 ? null : serializer.Serialize(parameters);
            //
            var record = $"SessionId: {actionContext.HttpContext.Session[Constants.SessionActivityLogIdKey]}, " +
                             $"UserName: {actionContext.HttpContext.User.Identity.Name}, " +
                             $"Controller: {actionContext.Controller.GetType().Name}, " +
                             $"Action: {actionContext.ActionDescriptor.ActionName}, " +
                             $"Parameters: {parameterString}";
            //
            Debug.WriteLine(record);
        }
    }
}