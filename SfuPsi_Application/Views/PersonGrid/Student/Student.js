﻿function initialiseStudentBlock(e) {
    var detailRow = e.detailRow[0];
    var commonReady = $(detailRow).find("[name='studentReady']");
    if (commonReady.val() == "1") return;

    // k1 date
    var k1DateInput = $(detailRow).find(".k1Date");
    k1DateInput.kendoDatePicker({
        format: "dd.MM.yyyy"
    });
    var k1Date = $(k1DateInput).data("kendoDatePicker");
    k1Date.enable(false);

    // k2 date
    var k2DateInput = $(detailRow).find(".k2Date");
    k2DateInput.kendoDatePicker({
        format: "dd.MM.yyyy"
    });
    var k2Date = $(k2DateInput).data("kendoDatePicker");
    k2Date.enable(false);

    // admission date
    var admissionDateInput = $(detailRow).find(".admissionDate");
    admissionDateInput.kendoDatePicker({
        format: "dd.MM.yyyy"
    });
    var admissionDate = $(admissionDateInput).data("kendoDatePicker");
    admissionDate.enable(false);

    // current level
    var currentLevelInput = $(detailRow).find(".currentLevel");
    currentLevelInput.kendoDropDownList({
        dataTextField: "Text",
        dataValueField: "Value",
        dataSource: Levels
    });
    var currentLevel = $(currentLevelInput).data("kendoDropDownList");
    currentLevel.enable(false);

    // program
    var programInput = $(detailRow).find(".program");
    programInput.kendoDropDownList({
        dataTextField: "Text",
        dataValueField: "Value",
        dataSource: Programs
    });
    var program = $(programInput).data("kendoDropDownList");
    program.enable(false);

    commonReady.val("1");
}

function studentEditClick(sender) {
    var personId = $(sender).attr("personId");
    var studentBlock = $(document).find(".studentBlock[personId=" + personId + "]");
    //
    var k1DateInput = studentBlock.find("input.k1Date");
    var k1Date = k1DateInput.data("kendoDatePicker");
    k1DateInput.attr("was", JSON.stringify(k1Date.value()));
    k1Date.enable(true);
    //
    var k2DateInput = studentBlock.find("input.k2Date");
    var k2Date = k2DateInput.data("kendoDatePicker");
    k2DateInput.attr("was", JSON.stringify(k2Date.value()));
    k2Date.enable(true);
    //
    var admissionDateInput = studentBlock.find("input.admissionDate");
    var admissionDate = admissionDateInput.data("kendoDatePicker");
    admissionDateInput.attr("was", JSON.stringify(admissionDate.value()));
    admissionDate.enable(true);
    //
    var currentLevelInput = studentBlock.find("input.currentLevel");
    var currentLevel = $(currentLevelInput).data("kendoDropDownList");
    currentLevelInput.attr("was", JSON.stringify(currentLevel.value()));
    currentLevel.enable(true);
    //
    var programInput = studentBlock.find("input.program");
    var program = $(programInput).data("kendoDropDownList");
    programInput.attr("was", JSON.stringify(program.value()));
    program.enable(true);
    //
    var textFields = studentBlock.find(".studentTextField");
    textFields.each(function() {
        var input = $(this);
        input.attr("was", input.val());
        input.removeAttr("disabled");
    });
    //
    studentBlock.find(".studentEdit").hide();
    studentBlock.find(".studentCancel").show();
    studentBlock.find(".studentSave").show();
    //
    return false;
}

function studentCancelClick(sender) {
    var personId = $(sender).attr("personId");
    var studentBlock = $(document).find(".studentBlock[personId=" + personId + "]");
    //
    var k1DateInput = studentBlock.find("input.k1Date");
    var k1Date = k1DateInput.data("kendoDatePicker");
    k1Date.value(k1DateInput.attr("was"));
    k1Date.enable(false);
    //
    var k2DateInput = studentBlock.find("input.k2Date");
    var k2Date = k2DateInput.data("kendoDatePicker");
    k2Date.value(k2DateInput.attr("was"));
    k2Date.enable(false);
    //
    var admissionDateInput = studentBlock.find("input.admissionDate");
    var admissionDate = admissionDateInput.data("kendoDatePicker");
    admissionDate.value(admissionDateInput.attr("was"));
    admissionDate.enable(false);
    //
    var currentLevelInput = studentBlock.find("input.currentLevel");
    var currentLevel = $(currentLevelInput).data("kendoDropDownList");
    currentLevel.value(currentLevelInput.attr("was"));
    currentLevel.enable(false);
    //
    var programInput = studentBlock.find("input.program");
    var program = $(programInput).data("kendoDropDownList");
    program.value(programInput.attr("was"));
    program.enable(false);
    //
    var textFields = studentBlock.find(".studentTextField");
    textFields.each(function () {
        var input = $(this);
        input.val(input.attr("was"));
        input.attr("disabled", "disabled");
    });
    //
    studentBlock.find(".studentEdit").show();
    studentBlock.find(".studentCancel").hide();
    studentBlock.find(".studentSave").hide();
    //
    return false;
}

function studentSaveClick(sender) {
    var personId = $(sender).attr("personId");
    var studentBlock = $(document).find(".studentBlock[personId=" + personId + "]");
    var studentForm = studentBlock.find(".studentForm");
    var formData = studentForm.serialize();
    var options = {
        url: studentForm.attr("action"),
        type: "POST",
        data: formData,
        success: function (responseJson) {
            if (responseJson.Success) {
                
                var personGrid = $("#PersonGrid").data("kendoGrid");
                var dataItem = personGrid.dataSource.get(personId);
                
                dataItem.set("StudentAdmissionDate", responseJson.AdmissionDate);
                dataItem.set("StudentAnalyst", responseJson.Analyst);
                dataItem.set("StudentSupervision", responseJson.Supervision);
                dataItem.set("StudentPreExam", responseJson.PreExam);
                dataItem.set("StudentK1Note", responseJson.K1Note);
                dataItem.set("StudentK1Date", responseJson.K1Date);
                dataItem.set("StudentK2Note", responseJson.K2Note);
                dataItem.set("StudentK2Date", responseJson.K2Date);
                dataItem.set("CurrentLevelId", responseJson.CurrentLevel);
                dataItem.set("ProgramId", responseJson.Program);

                ExpandRows("PersonGrid", function (row) {
                    return row.PersonId == personId;
                });

                $("#tabStrip_" + personId).data("kendoTabStrip").select(3);

            } else {
                alert(responseJson.Message);
            }
        },
        error: displayAjaxError
    };
    $.ajax(options);
    return false;
}