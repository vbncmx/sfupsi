﻿function contactEditClick(sender) {
    $(sender).closest(".commandLinkBlock").children().fadeToggle(1);
    var personId = $(sender).attr("personId");
    var contactForm = $(document).find(".contactForm[personId=" + personId + "]");
    
    var inputs = contactForm.find("input");
    inputs.each(function () {
        var input = $(this);
        input.removeAttr("disabled");
        input.attr("was", input.val());
    });

    return false;
}

function contactUpdateClick(sender) {

    var personId = $(sender).attr("personId");
    var contactForm = $(document).find(".contactForm[personId=" + personId + "]");
    var formData = contactForm.serialize();
    var options = {
        url: contactForm.attr("action"),
        type: "POST",
        data: formData,
        success: function (responseJson) {
            if (responseJson.Success) {

                var personGrid = $("#PersonGrid").data("kendoGrid");
                var dataItem = personGrid.dataSource.get(personId);

                dataItem.set("Email", responseJson.Email);
                dataItem.set("PhoneNumber1", responseJson.PhoneNumber1);
                dataItem.set("PhoneNumber2", responseJson.PhoneNumber2);
                dataItem.set("WebSite", responseJson.WebSite);
                dataItem.set("Country", responseJson.Country);
                dataItem.set("City", responseJson.City);
                dataItem.set("Street", responseJson.Street);
                dataItem.set("Zip", responseJson.Zip);

                ExpandRows("PersonGrid", function (row) {
                    return row.PersonId == personId;
                });

                $("#tabStrip_" + personId).data("kendoTabStrip").select(1);

            } else {
                alert(responseJson.Message);
            }
        },
        error: displayAjaxError
    };
    $.ajax(options);

    return false;
}

function contactCancelClick(sender) {
    $(sender).closest(".commandLinkBlock").children().fadeToggle(1);
    var personId = $(sender).attr("personId");
    var contactForm = $(document).find(".contactForm[personId=" + personId + "]");

    var inputs = contactForm.find("input");
    inputs.each(function () {
        var input = $(this);
        input.attr("disabled", "disabled");
        input.val(input.attr("was"));
    });

    return false;
}