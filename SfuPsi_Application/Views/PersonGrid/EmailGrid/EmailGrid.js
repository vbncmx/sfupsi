﻿function onChange() {
    var grid = $("#EmailGrid").data("kendoGrid");
    var rows = grid.select();
    var emailLine = "";
    rows.each(
        function () {
            var dataItem = grid.dataItem($(this));
            emailLine += dataItem.Email + ";";
        }
    );
    $("#selectedEmailsInput").val(emailLine);
}

function addRowHoverStyling() {
    $("table.k-selectable tbody tr").hover(function () {
        $(this).toggleClass("k-state-hover");
    });
};

