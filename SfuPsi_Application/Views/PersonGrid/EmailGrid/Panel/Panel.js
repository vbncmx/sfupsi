﻿function onFilterRoleChange() {
    var filterRoleSelect = $("#filterRoleSelect").data("kendoDropDownList");
    var filterRoleId = filterRoleSelect.value();
    $("#EmailGrid").data("kendoGrid").dataSource.read({ roleId: filterRoleId });
}

function selectedEmailsInputClick() {
    var emailLine = $("#selectedEmailsInput").val();
    $("#selectedEmailsInput").select();
    try {
        document.execCommand("copy");
        alert("Copied: " + emailLine.substring(0, 100) + " ...");
    } catch (err) {
        alert("Unable to copy, check browser support: https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand#Browser_compatibility");
    }
    return false;
}