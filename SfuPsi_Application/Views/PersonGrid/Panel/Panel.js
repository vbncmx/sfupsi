﻿function onFilterRoleChange() {
    $("#PersonGrid").data("kendoGrid").dataSource.read();
}

function clearFilter() {
    $(".k-filtercell button[type='button']").trigger("click");
}

function showListReport() {

    var reportTitle = prompt("Please give a title for the report:", "");
    if (reportTitle) {
        var gridSelector = "#PersonGrid";
        var dataSource = $(gridSelector).data("kendoGrid").dataSource;
        var filters = dataSource.filter();
        var allData = dataSource.data();
        var query = new kendo.data.Query(allData);
        var filteredData = query.filter(filters).data;

        var personIds = [];
        $.each(filteredData, function (index, item) {
            personIds.push(item.PersonId);
        });

        var personIdsString = personIds.toString();

        var url = "/Reporting/PersonListReport?reportTitle="
            + encodeURIComponent(reportTitle)
            + "&personIdsString=" + personIdsString;

        window.open(url, '_blank');
    }

    


    return false;

    

    // Construct email line & feedback message
    var emailLine = "";
    $.each(filteredData, function (index, item) {
        emailLine += fieldFunc(item) + ";";
    });

    return false;
}







