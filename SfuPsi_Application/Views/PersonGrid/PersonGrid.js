﻿function getReadPersonsParams() {
    var filterRoleSelect = $("#filterRoleSelect").
        data("kendoDropDownList");

    var selectionFilteringModeSelect = $("#selectionFilteringModeSelect").
        data("kendoDropDownList");

    if (typeof selectedPersonIds == "undefined") {
        selectedPersonIds = [];
    }

    return {
        roleId: filterRoleSelect.value(),
        selectedPersonIds: selectedPersonIds,
        selectionFilteringMode: selectionFilteringModeSelect.value()
    };
}

function selectPersonRow() {
    if (typeof selectedPersonIds == "undefined") {
        selectedPersonIds = [];
    }
    var row = $(this).closest("tr");
    var selectSpan = row.find("a.k-grid-Select>span");
    var personId = parseInt(row.attr("personId"));

    var personIdIndex = selectedPersonIds.indexOf(personId);
    if (personIdIndex > -1) {
        selectedPersonIds.splice(personIdIndex, 1);
        row.removeClass("personRowSelected");
        selectSpan.removeClass("k-i-cancel");
        selectSpan.addClass("k-i-tick");
    } else {
        selectedPersonIds.push(personId);
        row.addClass("personRowSelected");
        selectSpan.removeClass("k-i-tick");
        selectSpan.addClass("k-i-cancel");
    }

    var select = $("#selectionFilteringModeSelect").
        data("kendoDropDownList");
    var filteringMode = select.value();
    if (filteringMode === "showSelected") {
        $("#PersonGrid").data("kendoGrid").dataSource.read();
    }
}

function onSelectionFilteringModeChange() {
    $("#PersonGrid").data("kendoGrid").dataSource.read();
}

function personGridDataBound(e) {
    var rows = e.sender.tbody.children();

    for (var j = 0; j < rows.length; j++) {
        var row = $(rows[j]);

        var selectButton = row.find("a.k-grid-Select");
        var selectSpan = selectButton.find("span");
        var dataItem = e.sender.dataItem(row);
        var personId = dataItem.get("PersonId");
        row.attr("personId", personId);
        if (selectedPersonIds.indexOf(personId) > -1) {
            row.addClass("personRowSelected");
            selectSpan.addClass("k-icon k-i-cancel");
        } else {
            selectSpan.addClass("k-icon k-i-tick");
        }
        
        selectButton.click(selectPersonRow);
    }
}