﻿function initialiseProfileCommonBlock(e) {
    var detailRow = e.detailRow[0];
    var commonReady = $(detailRow).find("[name='commonReady']");
    if (commonReady.val() == "1") return;

    var commonDobInput = $(detailRow).find(".commonDob");
    commonDobInput.kendoDatePicker({
        format: "dd.MM.yyyy"
    });
    var commonDob = $(commonDobInput).data("kendoDatePicker");
    commonDob.enable(false);
    
    commonReady.val("1");
}

function commonSaveClick(sender) {
    var personId = $(sender).attr("personId");
    var commonForm = $(".commonForm[personId=" + personId + "]");
    var formData = commonForm.serialize();
    var options = {
        url: commonForm.attr("action"),
        type: "POST",
        data: formData,
        success: function (responseJson) {
            if (responseJson.Success) {
                // manage parent row (will refresh since bound to dataSource)
                var personGrid = $("#PersonGrid").data("kendoGrid");
                var dataItem = personGrid.dataSource.get(personId);
                dataItem.set("Name", responseJson.Name);
                dataItem.set("Title", responseJson.Title);
                dataItem.set("DateOfBirth", responseJson.Dob);
                dataItem.set("Languages", responseJson.Languages);

                ExpandRows("PersonGrid", function(row) {
                    return row.PersonId == personId;
                });

                // fix for photo rollback bug
                var profilePhotoSelector = ".profilePhoto[personId=" + personId + "]";
                $(profilePhotoSelector).attr("src", responseJson.PhotoUrl);
                profilePhotoSelector = "#photo_" + personId;
                $(profilePhotoSelector).attr("src", responseJson.PhotoUrl);

            } else {
                alert(responseJson.Message);
            }
        },
        error: displayAjaxError
    };
    $.ajax(options);
    return false;
}

function commonCancelClick(sender) {
    var personId = $(sender).attr("personId");
    var profileBlock = $(document).find(".profileBlock[personId=" + personId + "]");
    var commonDobInput = profileBlock.find("input.commonDob");
    var commonName = profileBlock.find(".commonName");
    var commonTitle = profileBlock.find(".commonTitle");
    var commonLanguages = profileBlock.find(".commonLanguages");
    var commonDob = commonDobInput.data("kendoDatePicker");

    // Dob
    commonDob.value(new Date(JSON.parse(commonDobInput.attr("was"))));
    commonDob.enable(false);

    // Name
    commonName.val(commonName.attr("was"));
    commonName.attr("disabled", "disabled");

    // Languages
    commonLanguages.val(commonLanguages.attr("was"));
    commonLanguages.attr("disabled", "disabled");

    // Title
    commonTitle.val(commonTitle.attr("was"));
    commonTitle.attr("disabled", "disabled");

    $(sender).closest(".commandLinkBlock").children().fadeToggle(1);

    return false;
}

function commonEditClick(sender) {
    var personId = $(sender).attr("personId");
    var profileBlock = $(document).find(".profileBlock[personId=" + personId + "]");
    var commonDobInput = profileBlock.find("input.commonDob");
    var commonName = profileBlock.find(".commonName");
    var commonLanguages = profileBlock.find(".commonLanguages");
    var commonTitle = profileBlock.find(".commonTitle");
    var commonDob = commonDobInput.data("kendoDatePicker");

    // Dob
    commonDob.enable(true);
    commonDobInput.attr("was", JSON.stringify(commonDob.value()));

    // Name
    commonName.removeAttr("disabled");
    commonName.attr("was", commonName.val());

    // Languages
    commonLanguages.removeAttr("disabled");
    commonLanguages.attr("was", commonTitle.val());

    // Title
    commonTitle.removeAttr("disabled");
    commonTitle.attr("was", commonTitle.val());

    $(sender).closest(".commandLinkBlock").children().fadeToggle(1);

    return false;
}