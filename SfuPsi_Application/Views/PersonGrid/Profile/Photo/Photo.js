﻿function profilePhotoRemoveClick(sender) {
    var personId = $(sender).attr("personId");
    var url = "/PersonGrid/RemoveProfilePhoto?personId=" + personId;
    $.ajax({
        type: "POST",
        url: url,
        success: function (response) {
            if (response.Success) {
                $("#photo_" + personId).attr("src", response.PhotoUrl);
                $(".profilePhoto[personId=" + personId + "]").attr("src", response.PhotoUrl);
                $(".profilePhotoRemove[personId=" + personId + "]").show();
                $(".profilePhotoChange[personId=" + personId + "]").show();
                $(".profilePhotoInput[personId=" + personId + "]").hide();
                $(".profilePhotoCancel[personId=" + personId + "]").hide();
            } else {
                alert(response.Message);
            }
        },
        error: displayAjaxError
    });
    return false;
}

function profilePhotoInputChange(sender) {
    var personId = $(sender).attr("personId");
    var url = "/PersonGrid/UpdateProfilePhoto?personId=" + personId;
    var file = sender.files[0];
    if (file == undefined) return;
    if (window.FormData == undefined) {
        alert("Browser does not suprofileort FormData.");
        return;
    }
    var data = new FormData();
    data.append("file0", file);
    $.ajax({
        type: "POST",
        url: url,
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            if (response.Success) {
                $("#photo_" + personId).attr("src", response.PhotoUrl);
                $(".profilePhoto[personId=" + personId + "]").attr("src", response.PhotoUrl);
                $(".profilePhotoRemove[personId=" + personId + "]").show();
                $(".profilePhotoChange[personId=" + personId + "]").show();
                $(".profilePhotoInput[personId=" + personId + "]").hide();
                $(".profilePhotoCancel[personId=" + personId + "]").hide();
            }
            else {
                alert(response.Message);
            }
        },
        error: displayAjaxError
    });
}

function profilePhotoChangeClick(sender) {
    var personId = $(sender).attr("personId");
    $(".profilePhotoRemove[personId=" + personId + "]").hide();
    $(".profilePhotoChange[personId=" + personId + "]").hide();
    $(".profilePhotoInput[personId=" + personId + "]").show();
    $(".profilePhotoCancel[personId=" + personId + "]").show();
    return false;
}

function profilePhotoCancelClick(sender) {
    var personId = $(sender).attr("personId");
    $(".profilePhotoRemove[personId=" + personId + "]").show();
    $(".profilePhotoChange[personId=" + personId + "]").show();
    $(".profilePhotoInput[personId=" + personId + "]").hide();
    $(".profilePhotoCancel[personId=" + personId + "]").hide();
    return false;
}