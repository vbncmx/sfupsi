﻿function noteEditClick(sender) {
    $(sender).closest(".commandLinkBlock").children().fadeToggle(1);
    var personId = $(sender).attr("personId");
    var profileNote = $(document).find(".profileNote[personId=" + personId + "]");
    profileNote.attr("was", profileNote.val());
    profileNote.removeAttr("disabled");
    return false;
}

function noteCancelClick(sender) {
    $(sender).closest(".commandLinkBlock").children().fadeToggle(1);
    var personId = $(sender).attr("personId");
    var profileNote = $(document).find(".profileNote[personId=" + personId + "]");
    profileNote.val(profileNote.attr("was"));
    profileNote.attr("disabled", "disabled");
    return false;
}

function noteSaveClick(sender) {
    var personId = $(sender).attr("personId");
    var noteForm = $(".noteForm[personId=" + personId + "]");
    var formData = noteForm.serialize();
    var options = {
        url: noteForm.attr("action"),
        type: "POST",
        data: formData,
        success: function (responseJson) {
            if (responseJson.Success) {
                
                var personGrid = $("#PersonGrid").data("kendoGrid");
                var dataItem = personGrid.dataSource.get(personId);
                dataItem.set("Note", responseJson.Note);
                dataItem.set("PhotoUrl", responseJson.PhotoUrl);

                ExpandRows("PersonGrid", function (row) {
                    return row.PersonId == personId;
                });

                // fix for photo rollback bug
                var profilePhotoSelector = ".profilePhoto[personId=" + personId + "]";
                $(profilePhotoSelector).attr("src", responseJson.PhotoUrl);
                profilePhotoSelector = "#photo_" + personId;
                $(profilePhotoSelector).attr("src", responseJson.PhotoUrl);

            } else {
                alert(responseJson.Message);
            }
        },
        error: displayAjaxError
    };
    $.ajax(options);
    return false;
}

