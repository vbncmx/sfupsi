﻿function onRoleGridDataBound() {
    var grid = $("#RoleGrid").data("kendoGrid");
    var gridData = grid.dataSource.view();

    // do not show 'Edit' & 'Remove' for built in items
    for (var i = 0; i < gridData.length; i++) {
        var currentUid = gridData[i].uid;
        if (gridData[i].BuiltIn === true) {
            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
            var editButton = $(currenRow).find(".k-grid-edit");
            editButton.hide();
            var deleteButton = $(currenRow).find(".k-grid-delete");
            deleteButton.hide();
        }
    }
}