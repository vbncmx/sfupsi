﻿function onUserGridDataBound() {
    var grid = $("#UserGrid").data("kendoGrid");
    var gridData = grid.dataSource.view();

    for (var i = 0; i < gridData.length; i++) {
        var currentUid = gridData[i].uid;
        if (gridData[i].IsAdmin === true) {
            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
            var deleteButton = $(currenRow).find(".k-grid-delete");
            deleteButton.hide();
        }
    }
}