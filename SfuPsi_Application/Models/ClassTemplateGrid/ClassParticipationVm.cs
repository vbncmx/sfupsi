﻿using System;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.ClassTemplateGrid
{
    public class ClassParticipationVm
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Note { get; set; }
        public int SemesterId { get; set; }

        public static ClassParticipationVm Construct(ClassParticipation participation)
        {
            return new ClassParticipationVm
            {
                Id = participation.Id,
                Note = participation.Note,
                PersonId = participation.PersonId,
                SemesterId = participation.SemesterId ?? 0
            };
        }

        public ClassParticipation Apply(ClassParticipation participation)
        {
            participation.PersonId = PersonId;
            participation.Note = Note;
            participation.SemesterId = SemesterId == 0 ? null : (int?)SemesterId;
            return participation;
        }
    }
}