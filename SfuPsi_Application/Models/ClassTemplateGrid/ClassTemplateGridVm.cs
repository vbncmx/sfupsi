﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SfuPsi_Application.Models.ListItems;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Models.ClassTemplateGrid
{
    public class ClassTemplateGridVm
    {
        public SelectList Submodules { get; set; }
        public SelectList Levels { get; set; }
        public SelectList Programs { get; set; }
        public SelectList Persons { get; set; }
        public SelectList Semesters { get; set; }

        public static async Task<ClassTemplateGridVm> Construct(SfuPsiContext context)
        {
            return new ClassTemplateGridVm
            {
                Submodules = await LoadSubmodules(context),
                Levels = await LoadLevels(context),
                Programs = await LoadPrograms(context),
                Persons = await LoadPersons(context),
                Semesters = await LoadSemesters(context)
            };
        }

        private static async Task<SelectList> LoadLevels(SfuPsiContext context)
        {
            var levels = await context.Levels.ToArrayAsync();
            return new SelectList(levels, nameof(Level.LevelId), nameof(Level.LevelTitle));
        }

        private static async Task<SelectList> LoadSubmodules(SfuPsiContext context)
        {
            var submodules = (await context.SubModules.ToArrayAsync())
                .Select(s => new SubModuleLi(s))
                .Concat(new[] { new SubModuleLi { Id = 0, Title = "-" }, })
                .ToArray();
            return new SelectList(submodules,
                    nameof(SubModuleLi.Id),
                    nameof(SubModuleLi.Title));
        }

        private static async Task<SelectList> LoadPrograms(SfuPsiContext context)
        {
            var programs = await context.Programs.ToArrayAsync();
            var programsLis = new[] { ProgramLi.Empty() }.Concat(programs.Select(ProgramLi.FromEntity));
            return new SelectList(programsLis, nameof(ProgramLi.Id), nameof(ProgramLi.Title));
        }

        private static async Task<SelectList> LoadPersons(SfuPsiContext context)
        {
            var persons = await context.Persons.OrderBy(p => p.Name).ToArrayAsync();
            var personLis = persons.Select(PersonLi.Construct);
            return new SelectList(personLis, nameof(PersonLi.PersonId), nameof(PersonLi.PersonName));
        }

        private static async Task<SelectList> LoadSemesters(SfuPsiContext context)
        {
            var semesters = await context.Semesters.ToArrayAsync();
            var semesterLis = new[] { SemesterLi.Empty() }.Concat(semesters.Select(SemesterLi.FromEntity));
            return new SelectList(semesterLis, nameof(SemesterLi.Id), nameof(SemesterLi.Title));
        }
    }
}