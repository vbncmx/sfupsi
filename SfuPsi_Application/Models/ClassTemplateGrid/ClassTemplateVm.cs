﻿using System.ComponentModel.DataAnnotations;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.ClassTemplateGrid
{
    public class ClassTemplateVm
    {
        public static ClassTemplateVm Construct(ClassTemplate classTemplate)
        {
            return new ClassTemplateVm
            {
                ClassTemplateId = classTemplate.ClassTemplateId,
                ClassTemplateTitle = classTemplate.ClassTemplateTitle,
                LevelId = classTemplate.LevelId,
                SubModuleId = classTemplate.ClassSubModuleId ?? 0,
                ClassTitle = classTemplate.ClassTitle,
                ClassDescription = classTemplate.ClassDescription,
                ClassMethodology = classTemplate.ClassMethodology,
                ClassSuccessCondition = classTemplate.ClassSuccessCondition,
                ClassLinks = classTemplate.ClassLinks,
                ClassRecommendedLiterature = classTemplate.ClassRecommendedLiterature,
                ClassProfessorComment = classTemplate.ClassProfessorComment,
                ProgramId = classTemplate.ProgramId ?? 0
            };
        }

        public int ClassTemplateId { get; set; }
        [Required]
        public string ClassTemplateTitle { get; set; }
        public int LevelId { get; set; }
        public int SubModuleId { get; set; }
        public string ClassTitle { get; set; }
        public string ClassDescription { get; set; }
        public string ClassMethodology { get; set; }
        public string ClassSuccessCondition { get; set; }
        public string ClassLinks { get; set; }
        public string ClassRecommendedLiterature { get; set; }
        public string ClassProfessorComment { get; set; }
        public int ProgramId { get; set; }

        public ClassTemplate Apply(ClassTemplate classTemplate)
        {
            classTemplate.ClassTemplateTitle = ClassTemplateTitle;
            classTemplate.LevelId = LevelId;
            classTemplate.ClassSubModuleId = SubModuleId == 0 ? null : (int?) SubModuleId;
            classTemplate.ClassTitle = ClassTitle;
            classTemplate.ClassDescription = ClassDescription;
            classTemplate.ClassMethodology = ClassMethodology;
            classTemplate.ClassSuccessCondition = ClassSuccessCondition;
            classTemplate.ClassLinks = ClassLinks;
            classTemplate.ClassRecommendedLiterature = ClassRecommendedLiterature;
            classTemplate.ClassProfessorComment = ClassProfessorComment;
            classTemplate.ProgramId = ProgramId == 0 ? null : (int?) ProgramId;
            return classTemplate;
        }
    }
}