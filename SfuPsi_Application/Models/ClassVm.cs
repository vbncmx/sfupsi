﻿using System.ComponentModel.DataAnnotations;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class ClassVm
    {
        public int ClassId { get; set; }
        public int LevelId { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }
        public string Methodology { get; set; }
        public string SuccessCondition { get; set; }
        public string Links { get; set; }
        public string RecommendedLiterature { get; set; }
        public string ProfessorComment { get; set; }
        public int SubModuleId { get; set; }

        public ClassVm(){ }

        public ClassVm(Class classEntity)
        {
            ClassId = classEntity.ClassId;
            LevelId = classEntity.LevelId;
            Title = classEntity.Title;
            Description = classEntity.Description;
            Methodology = classEntity.Methodology;
            SuccessCondition = classEntity.SuccessCondition;
            Links = classEntity.Links;
            RecommendedLiterature = classEntity.RecommendedLiterature;
            ProfessorComment = classEntity.ProfessorComment;
            SubModuleId = classEntity.SubModuleId ?? 0;
        }

        public void Apply(Class classEntity)
        {
            classEntity.LevelId = LevelId;
            classEntity.Title = Title;
            classEntity.Description = Description;
            classEntity.Methodology = Methodology;
            classEntity.SuccessCondition = SuccessCondition;
            classEntity.Links = Links;
            classEntity.RecommendedLiterature = RecommendedLiterature;
            classEntity.ProfessorComment = ProfessorComment;
            classEntity.SubModuleId = SubModuleId == 0 ? null : (int?) SubModuleId;
        }
    }
}