﻿using System.Linq;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class RoleVm
    {
        public RoleVm() { }

        public RoleVm(Role role)
        {
            RoleId = role.RoleId;
            RoleTitle = role.RoleTitle;
            BuiltIn = Role.BuiltInRoleIds.Contains(role.RoleId);
            Order = role.Order;
        }

        public int RoleId { get; set; }
        public string RoleTitle { get; set; }
        public bool BuiltIn { get; set; }
        public int Order { get; set; }
    }
}