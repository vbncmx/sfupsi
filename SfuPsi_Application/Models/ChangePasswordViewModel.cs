using System.ComponentModel.DataAnnotations;
using SfuPsi_Application.Identity;

namespace SfuPsi_Application.Models
{
    public class ChangePasswordViewModel
    {
        [StringLength(255, ErrorMessage = "Must be at least 5 characters", MinimumLength = AppUser.MinPasswordLength)]
        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [StringLength(255, ErrorMessage = "Must be at least 5 characters", MinimumLength = AppUser.MinPasswordLength)]
        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        [Required]
        [Display(Name = "New Password Confirm")]
        public string NewPasswordConfirm { get; set; }

        [Required]
        [Display(Name = "Current Password")]
        public string CurrentPassword { get; set; }

        public bool Validate { get; set; }
    }
}