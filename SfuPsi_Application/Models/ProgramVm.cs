﻿using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class ProgramVm
    {
        public ProgramVm() { }

        public ProgramVm(Program program)
        {
            if (program == null) return;
            ProgramId = program.ProgramId;
            ProgramTitle = program.Title;
        }

        public int ProgramId { get; set; }
        public string ProgramTitle { get; set; } 
    }
}