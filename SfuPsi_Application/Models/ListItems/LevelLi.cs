﻿using System.Security.Cryptography;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.ListItems
{
    public class LevelLi
    {
        public int LevelId { get; private set; }
        public string LevelTitle { get; private set; }

        public static LevelLi FromEntity(Level level)
        {
            return new LevelLi
            {
                LevelId = level.LevelId,
                LevelTitle = level.LevelTitle
            };
        }

        public static LevelLi Empty()
        {
            return new LevelLi
            {
                LevelId = 0,
                LevelTitle = "-"
            };
        }
    }
}