﻿using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.ListItems
{
    public class SubModuleLi
    {
        public SubModuleLi() {  }
        public SubModuleLi(SubModule subModule)
        {
            Id = subModule.SubModuleId;
            Title = $"{subModule.Module?.ModuleTitle}:{subModule.Title}";
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public static SubModuleLi Null()
        {
            return new SubModuleLi
            {
                Id= 0,
                Title = "Null"
            };
        }
    }
}