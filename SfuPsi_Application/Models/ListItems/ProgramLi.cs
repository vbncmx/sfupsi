using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.ListItems
{
    public class ProgramLi
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public static ProgramLi FromEntity(Program program)
        {
            return new ProgramLi()
            {
                Id = program.ProgramId,
                Title = program.Title
            };
        }

        public static ProgramLi Empty()
        {
            return new ProgramLi()
            {
                Id = 0,
                Title = "-"
            };
        }
    }
}