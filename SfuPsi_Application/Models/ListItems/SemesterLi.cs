﻿using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.ListItems
{
    public class SemesterLi
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public static SemesterLi FromEntity(Semester semester)
        {
            return new SemesterLi
            {
                Id = semester.Id,
                Title = semester.Title
            };
        }

        public static SemesterLi Empty()
        {
            return new SemesterLi
            {
                Id = 0,
                Title = "-"
            };
        }
    }
}