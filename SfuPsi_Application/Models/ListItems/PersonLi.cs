﻿using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.ListItems
{
    public class PersonLi
    {
        public int PersonId { get; private set; }
        public string PersonName { get; private set; }

        public static PersonLi Construct(Person person)
        {
            return new PersonLi
            {
                PersonId = person.PersonId,
                PersonName = person.Name
            };
        }

        public static PersonLi Empty()
        {
            return new PersonLi
            {
                PersonId = 0,
                PersonName = "-"
            };
        }

        
    }
}