﻿using System.Collections.Generic;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class PersonGridModel
    {
        public PersonGridModel(IEnumerable<Person> persons)
        {
            Persons = persons;
        }

        public IEnumerable<Person> Persons { get; private set; }
    }
}