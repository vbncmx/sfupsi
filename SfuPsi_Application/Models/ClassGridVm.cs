﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SfuPsi_Application.Models.ListItems;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Models
{
    public class ClassGridVm
    {
        public static async Task<ClassGridVm> FromContext(SfuPsiContext context)
        {
            var classGridVm = new ClassGridVm();

            var levels = await context.Levels.ToArrayAsync();
            classGridVm.Levels = new SelectList(levels, nameof(Level.LevelId), nameof(Level.LevelTitle));

            var submodules = await context.SubModules.ToArrayAsync();
            var submodulesLis = submodules.Select(s => new SubModuleLi(s))
                .Concat(new[] { SubModuleLi.Null() })
                .ToArray();
            classGridVm.Submodules = new SelectList(submodulesLis, nameof(SubModuleLi.Id), nameof(SubModuleLi.Title));

            var professors = await context.PersonRoles.Where(r => r.RoleId == Role.ProfessorRoleId)
                .Select(r => r.Person)
                .ToArrayAsync();
            var professorLis = professors.Select(PersonLi.Construct);
            classGridVm.Professors = new SelectList(professorLis, nameof(PersonLi.PersonId), nameof(PersonLi.PersonName));

            return classGridVm;
        }

        public SelectList Levels { get; set; }
        public SelectList Submodules { get; set; }
        public SelectList Professors { get; set; }
    }
}