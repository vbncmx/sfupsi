﻿using System.Threading.Tasks;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.LevelGrid
{
    public class LevelVm
    {
        public int LevelId { get; set; }
        public string Title { get; set; }

        public static LevelVm FromEntity(Level level)
        {
            return new LevelVm
            {
                LevelId = level.LevelId,
                Title = level.LevelTitle
            };
        }

        public Level Apply(Level level)
        {
            level.LevelTitle = Title;
            return level;
        }
    }
}