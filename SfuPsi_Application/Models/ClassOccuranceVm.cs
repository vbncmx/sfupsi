﻿using System;
using SfuPsi_Application.Models.Common;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class ClassOccuranceVm
    {
        public ClassOccuranceVm() { }

        public ClassOccuranceVm(ClassOccurance classOccurance)
        {
            ClassOccuranceId = classOccurance.ClassOccuranceId;
            Date = classOccurance.Date;
            StartTime = new TimeVm(classOccurance.StartTime);
            Duration = new TimeVm(classOccurance.Duration);
        }

        public int ClassOccuranceId { get; set; }
        public DateTime Date { get; set; }
        public TimeVm StartTime { get; set; }
        public TimeVm Duration { get; set; }

        public ClassOccurance Apply(ClassOccurance classOccurance)
        {
            classOccurance.Date = Date;
            classOccurance.StartTime = StartTime.ToTimeSpan();
            classOccurance.Duration = Duration.ToTimeSpan();
            return classOccurance;
        }
    }
}