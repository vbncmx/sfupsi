﻿using System.ComponentModel.DataAnnotations;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class ModuleViewModel
    {
        public ModuleViewModel() { }

        public ModuleViewModel(Module module)
        {
            if (module == null) return;
            ModuleId = module.ModuleId;
            ModuleTitle = module.ModuleTitle;
            ProgramId = module.ProgramId;
            ProgramTitle = module.Program?.Title;
            SubModuleCount = module.SubModules.Count;
        }

        public int ModuleId { get; set; }

        [Required]
        public string ModuleTitle { get; set; }

        [Required]
        public int ProgramId { get; set; }

        public string ProgramTitle { get; set; }

        public int SubModuleCount { get; set; }

        public Module ToModule()
        {
            return new Module
            {
                ModuleId = ModuleId,
                ModuleTitle = ModuleTitle,
                ProgramId = ProgramId
            };
        }
    }
}