﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using SfuPsi_Application.Identity;

namespace SfuPsi_Application.Models
{
    public class UserVm
    {
        public UserVm() { }

        public UserVm(AppUser appUser)
        {
            Id = appUser.Id;
            UserName = appUser.UserName;
            Email = appUser.Email;
            IsAdmin = appUser.Roles.Any(r => r.RoleId == AppRole.AdminRoleId);
        }

        public string Id { get; set; }

        [Required]
        public string UserName { get; set; }

        public string Email { get; set; }

        public bool IsAdmin { get; set; }
    }
}