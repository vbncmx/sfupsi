﻿using System;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class ClassProfessorVm
    {
        public ClassProfessorVm() { }

        public ClassProfessorVm(Person person)
        {
            PersonId = person.PersonId;
            ProfessorId = Guid.NewGuid().ToString();
        }


        public string ProfessorId { get; set; }
        public int PersonId { get; set; }
    }
}