﻿using System;

namespace SfuPsi_Application.Models.Common
{
    public class TimeVm
    {
        public TimeVm(int hours, int minutes)
        {
            Hours = hours;
            Minutes = minutes;
        }

        public TimeVm() : this(0, 0) { }

        public TimeVm(TimeSpan timeSpan) : this(timeSpan.Hours, timeSpan.Minutes) { }

        public int Hours { get; set; }
        public int Minutes { get; set; }

        public TimeSpan ToTimeSpan()
        {
            return new TimeSpan(Hours, Minutes, 0);
        }
    }

    
}