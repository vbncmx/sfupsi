﻿namespace SfuPsi_Application.Models
{
    public class EmailVm
    {
        public EmailVm() { }

        public EmailVm(string email, string personName)
        {
            Email = email;
            PersonName = personName;
        }

        public string PersonName { get; set; }
        public string Email { get; set; }
    }
}