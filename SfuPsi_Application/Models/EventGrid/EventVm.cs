﻿using System;
using System.ComponentModel.DataAnnotations;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.EventGrid
{
    public class EventVm
    {
        public int EventId { get; set; }

        [Required]
        public string Title { get; set; }

        public DateTime? Date { get; set; }

        public string Note { get; set; }

        public static EventVm Construct(Event @event)
        {
            return new EventVm
            {
                EventId = @event.Id,
                Title = @event.Title,
                Date = @event.Date,
                Note = @event.Note
            };
        }

        public Event Apply(Event @event)
        {
            @event.Title = Title;
            @event.Date = Date;
            @event.Note = Note;
            return @event;
        }
    }
}