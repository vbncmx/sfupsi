﻿using System;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.EventGrid
{
    public class EventParticipationVm
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Note { get; set; }

        public static EventParticipationVm Construct(EventParticipation participation)
        {
            return new EventParticipationVm
            {
                Id = participation.Id,
                Note = participation.Note,
                PersonId = participation.PersonId
            };
        }

        public EventParticipation Apply(EventParticipation participation)
        {
            participation.Note = Note;
            participation.PersonId = PersonId;
            return participation;
        }
    }
}