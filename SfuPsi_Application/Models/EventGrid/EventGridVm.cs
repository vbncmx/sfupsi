﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SfuPsi_Application.Models.ListItems;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Models.EventGrid
{
    public class EventGridVm
    {
        public SelectList Persons { get; set; }

        public static async Task<EventGridVm> Construct(SfuPsiContext context)
        {
            return new EventGridVm
            {
                Persons = await LoadPersons(context)
            };
        }

        private static async Task<SelectList> LoadPersons(SfuPsiContext context)
        {
            var persons = await context.Persons.OrderBy(p => p.Name).ToArrayAsync();
            var personLis = persons.Select(PersonLi.Construct);
            return new SelectList(personLis, nameof(PersonLi.PersonId), nameof(PersonLi.PersonName));
        }
    }
}