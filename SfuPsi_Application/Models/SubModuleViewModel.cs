﻿using System;
using System.ComponentModel.DataAnnotations;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models
{
    public class SubModuleViewModel
    {
        public SubModuleViewModel() { }

        public SubModuleViewModel(SubModule subModule)
        {
            SubModuleId = subModule.SubModuleId;
            SubModuleTitle = subModule.Title;
            DetailedTitle = $"{subModule.Module?.ModuleTitle}:{subModule.Title}";
        }

        public int SubModuleId { get; set; }

        [Required]
        public string SubModuleTitle { get; set; }

        public string DetailedTitle { get; set; }

        public SubModule ToSubModule()
        {
            return new SubModule
            {
                SubModuleId = SubModuleId,
                Title = SubModuleTitle
            };
        }
    }
}