﻿using System.Web.Mvc;

namespace SfuPsi_Application.Models
{
    public class ModuleGridVm
    {
        public SelectList Programs { get; set; }
    }
}