﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using SfuPsi_Application.Identity;
using SfuPsi_Application.Models.Common;

namespace SfuPsi_Application.Models
{
    public class ActionLogRecordVm
    {
        public string UserName { get; set; }

        public string SessionId { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Parameters { get; set; }

        public DateTime Date { get; set; }

        public string Time { get; set; }

        public static ActionLogRecordVm FromEntity(ActionLogRecord record)
        {
            return new ActionLogRecordVm
            {
                UserName = record.UserName,
                SessionId = record.SessionId,
                Controller = record.Controller,
                Action = record.Action,
                Parameters = record.Parameters,
                Date = record.DateTime.Date,
                Time = record.DateTime.TimeOfDay.ToString(@"hh\:mm")
            };
        }
    }
}