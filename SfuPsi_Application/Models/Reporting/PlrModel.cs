﻿using System.Collections.Generic;
using System.Linq;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Models.Reporting
{
    public class PlrModel
    {
        public List<PlrRow> Rows { get; set; }

        public static PlrModel Construct(int[] personIds, string reportTitle, SfuPsiContext context)
        {
            return new PlrModel
            {
                Rows = context.Persons
                    .Where(p => personIds.Contains(p.PersonId))
                    .ToArray()
                    .Select(p => PlrRow.Construct(p, reportTitle))
                    .ToList()
            };
        }
    }
}