﻿using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.Reporting
{
    public class PlrRow
    {
        public string PhotoUrl { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ReportTitle { get; set; }

        public static PlrRow Construct(Person person, string reportTitle)
        {
            return new PlrRow
            {
                Name = person.Name,
                Title = person.Title,
                PhotoUrl = person.PhotoUrl(),
                Email = person.Email,
                ReportTitle = reportTitle
            };
        }
    }
}