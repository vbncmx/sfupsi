﻿using System.ComponentModel.DataAnnotations;
using SfuPsi_Application.Identity;

namespace SfuPsi_Application.Models
{
    public class AccountViewModel
    {
        public AccountViewModel() { }

        public AccountViewModel(AppUser appUser)
        {
            Email = appUser.Email;
        }

        [EmailAddress]
        public string Email { get; set; }

        public ChangePasswordViewModel ChangePasswordVm { get; set; }
    }
}