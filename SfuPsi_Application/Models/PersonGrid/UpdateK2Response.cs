﻿using System;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class UpdateK2Response
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public DateTime? Date { get; set; }
        public string Note { get; set; }

        public static UpdateK2Response SuccessfulResponse(DateTime? date, string note)
        {
            return new UpdateK2Response
            {
                Success = true,
                Date = date,
                Message = "Updated",
                Note = note
            };
        }

        public static UpdateK2Response ErrorResponse(Exception exc)
        {
            return new UpdateK2Response
            {
                Success = false,
                Message = $"Server Error: {exc}"
            };
        }
    }
}