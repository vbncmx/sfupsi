using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class UpdateStudentDataRequest
    {
        public string AdmissionDate { get; set; }
        public string Analyst { get; set; }
        public string Supervision { get; set; }
        public string PreExam { get; set; }
        public string K1Note { get; set; }
        public string K1Date { get; set; }
        public string K2Note { get; set; }
        public string K2Date { get; set; }
        public int CurrentLevel { get; set; }
        public int Program { get; set; }

        public Person Apply(Person person)
        {
            person.StudentAdmissionDate = AdmissionDate.ParseAsDate();
            person.StudentAnalyst = Analyst;
            person.StudentSupervision = Supervision;
            person.StudentPreExam = PreExam;
            person.StudentK1 = K1Note;
            person.StudentK1Date = K1Date.ParseAsDate();
            person.StudentK2 = K2Note;
            person.StudentK2Date = K2Date.ParseAsDate();
            person.CurrentLevelId = CurrentLevel == 0 ? null : (int?) CurrentLevel;
            person.ProgramId = Program == 0 ? null : (int?) Program;
            return person;
        }
    }
}