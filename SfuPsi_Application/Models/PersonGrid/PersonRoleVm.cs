﻿using System;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class PersonRoleVm
    {
        public PersonRoleVm() { }

        public PersonRoleVm(PersonRole personRole)
        {
            PersonRoleId = personRole.PersonRoleId;
            RoleId = personRole.RoleId;
            StartDate = personRole.StartDate;
            EndDate = personRole.EndDate;
        }

        public int PersonRoleId { get; set; }
        public int RoleId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}