﻿using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class UpdateContactRequest
    {
        public int PersonId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Street { get; set; }
        public string Web { get; set; }
        public string Email { get; set; }

        public Person Apply(Person person)
        {
            person.PhoneNumber1 = Phone1;
            person.PhoneNumber2 = Phone2;
            person.Country = Country;
            person.City = City;
            person.Zip = Zip;
            person.Street = Street;
            person.WebSite = Web;
            person.Email = Email;
            return person;
        }
    }
}