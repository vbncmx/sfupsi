﻿using System;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class UploadPhotoResponse
    {
        public string PhotoUrl { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

        public static UploadPhotoResponse SuccessfullResponse(string photoUrl)
        {
            return new UploadPhotoResponse
            {
                Success = true,
                Message = "Updated",
                PhotoUrl = photoUrl
            };
        }

        public static UploadPhotoResponse ErrorResponse(Exception exc)
        {
            return new UploadPhotoResponse
            {
                Success = false,
                Message = $"Server Error: {exc}"
            };
        }
    }
}