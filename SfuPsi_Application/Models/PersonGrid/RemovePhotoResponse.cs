﻿using System;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class RemovePhotoResponse
    {
        public string PhotoUrl { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

        public static RemovePhotoResponse SuccessfullResponse()
        {
            return new RemovePhotoResponse
            {
                Success = true,
                Message = "Removed",
                PhotoUrl = $"/Images/nophoto.jpg?{DateTime.Now}"
            };
        }

        public static RemovePhotoResponse ErrorResponse(Exception exc)
        {
            return new RemovePhotoResponse
            {
                Success = false,
                Message = $"Server Error: {exc}"
            };
        }
    }
}