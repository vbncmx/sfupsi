﻿using System;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class UpdateContactResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Street { get; set; }
        public string WebSite { get; set; }
        public string Email { get; set; }

        public static UpdateContactResponse SuccessfullResponse(UpdateContactRequest request)
        {
            return new UpdateContactResponse
            {
                Success = true,
                Message = "Updated",
                PhoneNumber1 = request.Phone1,
                PhoneNumber2 = request.Phone2,
                Country = request.Country,
                City = request.City,
                Zip = request.Zip,
                Street = request.Street,
                WebSite = request.Web,
                Email = request.Email
            };
        }

        public static UpdateContactResponse ErrorResponse(Exception exc)
        {
            return new UpdateContactResponse
            {
                Success = false,
                Message = $"Server Error: {exc}"
            };
        }
    }
}