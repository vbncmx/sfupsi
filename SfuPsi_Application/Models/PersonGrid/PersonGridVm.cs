﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SfuPsi_Application.Models.ListItems;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class PersonGridVm
    {
        public const int RoleFilterAllValue = 0;

        public SelectList Roles { get; private set; }
        public SelectList Levels { get; set; }
        public SelectList Programs { get; set; }
        public int[] StudentIds { get; set; }
        public SelectListItem[] RoleFilterList { get; set; }

        public static async Task<PersonGridVm> FromContext(SfuPsiContext context)
        {
            var roles = await context.Roles.OrderBy(r => r.Order).ToArrayAsync();

            return new PersonGridVm
            {
                Roles = new SelectList(roles, nameof(Role.RoleId), nameof(Role.RoleTitle)),
                StudentIds = await LoadStudentIds(context),
                RoleFilterList = ConstructRoleFilterList(roles),
                Levels = await ConstructLevelList(context),
                Programs = await ConstructPrograms(context)
            };
        }

        private static async Task<int[]> LoadStudentIds(SfuPsiContext context)
        {
            return await context.PersonRoles.Where(r => r.RoleId == Role.StudentRoleId)
                .Select(r => r.PersonId)
                .ToArrayAsync();
        }

        public static SelectListItem[] ConstructRoleFilterList(Role[] roles)
        {
            return roles.Select(r => new SelectListItem
            {
                Text = r.RoleTitle,
                Value = r.RoleId.ToString()
            })
                .Concat(new[] {new SelectListItem {Text = "All", Value = $"{RoleFilterAllValue}"}})
                .ToArray();
        }

        public static async Task<SelectList> ConstructLevelList(SfuPsiContext context)
        {
            var levels = await context.Levels.ToArrayAsync();
            var levelLis = new[] {LevelLi.Empty()}.Concat(levels.Select(LevelLi.FromEntity));
            return new SelectList(levelLis, nameof(LevelLi.LevelId), nameof(LevelLi.LevelTitle));
        }

        public static async Task<SelectList> ConstructPrograms(SfuPsiContext context)
        {
            var programs = await context.Programs.ToArrayAsync();
            var programsLis = new[] {ProgramLi.Empty()}.Concat(programs.Select(ProgramLi.FromEntity));
            return new SelectList(programsLis, nameof(ProgramLi.Id), nameof(ProgramLi.Title));
        }
    }
}