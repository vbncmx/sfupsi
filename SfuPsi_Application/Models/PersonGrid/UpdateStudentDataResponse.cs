﻿using System;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class UpdateStudentDataResponse
    {
        public DateTime? AdmissionDate { get; set; }
        public string Analyst { get; set; }
        public string Supervision { get; set; }
        public string PreExam { get; set; }
        public string K1Note { get; set; }
        public DateTime? K1Date { get; set; }
        public string K2Note { get; set; }
        public DateTime? K2Date { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public int CurrentLevel { get; set; }
        public int Program { get; set; }

        public static UpdateStudentDataResponse Successful(UpdateStudentDataRequest request)
        {
            return new UpdateStudentDataResponse
            {
                AdmissionDate = request.AdmissionDate.ParseAsDate(),
                Analyst = request.Analyst,
                Supervision = request.Supervision,
                PreExam = request.PreExam,
                K1Date = request.K1Date.ParseAsDate(),
                K1Note = request.K1Note,
                K2Date = request.K2Date.ParseAsDate(),
                K2Note = request.K2Note,
                CurrentLevel = request.CurrentLevel,
                Program = request.Program,
                Success = true
            };
        }

        public static UpdateStudentDataResponse Failed(Exception exc)
        {
            return new UpdateStudentDataResponse
            {
                Success = false,
                Message = exc.ToString()
            };
        }
    }
}