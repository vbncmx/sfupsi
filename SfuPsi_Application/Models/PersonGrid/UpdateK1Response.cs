﻿using System;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class UpdateK1Response
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public DateTime? Date { get; set; }
        public string Note { get; set; }

        public static UpdateK1Response SuccessfulResponse(DateTime? date, string note)
        {
            return new UpdateK1Response
            {
                Success = true,
                Date = date,
                Message = "Updated",
                Note = note
            };
        }

        public static UpdateK1Response ErrorResponse(Exception exc)
        {
            return new UpdateK1Response
            {
                Success = false,
                Message = $"Server Error: {exc}"
            };
        }
    }
}