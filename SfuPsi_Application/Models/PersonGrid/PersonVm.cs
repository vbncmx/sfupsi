﻿using System;
using System.ComponentModel.DataAnnotations;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.PersonGrid
{
    public class PersonVm
    {
        public static PersonVm Construct(Person person)
        {
            return new PersonVm
            {
                // Profile
                PersonId = person.PersonId,
                Name = person.Name,
                Email = person.Email,
                DateOfBirth = person.DateOfBirth,
                Title = person.Title,
                PhotoUrl = person.PhotoUrl(),
                Note = person.Note,
                Languages = person.Languages,

                // Contacts
                PhoneNumber1 = person.PhoneNumber1,
                PhoneNumber2 = person.PhoneNumber2,
                Country = person.Country,
                City = person.City,
                Zip = person.Zip,
                Street = person.Street,
                WebSite = person.WebSite,

                // Student Data
                StudentAdmissionDate = person.StudentAdmissionDate,
                StudentAnalyst = person.StudentAnalyst,
                StudentSupervision = person.StudentSupervision,
                StudentPreExam = person.StudentPreExam,
                StudentK1Note = person.StudentK1,
                StudentK1Date = person.StudentK1Date?.Date,
                StudentK2Note = person.StudentK2,
                StudentK2Date = person.StudentK2Date?.Date,
                CurrentLevelId = person.CurrentLevelId ?? 0,
                ProgramId = person.ProgramId ?? 0
            };
        }

        public int PersonId { get; set; }

        // Profile
        [Required]
        public string Name { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Title { get; set; }
        public string PhotoUrl { get; set; }
        public string Note { get; set; }
        public string Languages { get; set; }

        // Contacts
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Street { get; set; }
        public string WebSite { get; set; }

        // Student Data
        public DateTime? StudentAdmissionDate { get; set; }
        public string StudentAnalyst { get; set; }
        public string StudentSupervision { get; set; }
        public string StudentPreExam { get; set; }
        public string StudentK1Note { get; set; }
        public DateTime? StudentK1Date { get; set; }
        public string StudentK2Note { get; set; }
        public DateTime? StudentK2Date { get; set; }
        public int CurrentLevelId { get; set; }
        public int ProgramId { get; set; }

        public Person Apply(Person person)
        {
            person.SetName(Name);
            person.Email = Email;
            person.DateOfBirth = DateOfBirth;
            person.Title = Title;
            person.Note = Note;
            person.PhoneNumber1 = PhoneNumber1;
            person.PhoneNumber2 = PhoneNumber2;
            person.Country = Country;
            person.City = City;
            person.Zip = Zip;
            person.Street = Street;
            person.WebSite = WebSite;
            person.StudentAdmissionDate = StudentAdmissionDate;
            person.StudentAnalyst = StudentAnalyst;
            person.StudentSupervision = StudentSupervision;
            person.StudentPreExam = StudentPreExam;
            person.Languages = Languages;
            //
            person.StudentK1 = StudentK1Note;
            person.StudentK1Date = StudentK1Date;
            //
            person.StudentK2 = StudentK2Note;
            person.StudentK2Date = StudentK2Date;
            person.CurrentLevelId = CurrentLevelId == 0
                ? null
                : (int?)CurrentLevelId;
            person.ProgramId = ProgramId == 0
                ? null
                : (int?)ProgramId;
            //
            return person;
        }
    }
}