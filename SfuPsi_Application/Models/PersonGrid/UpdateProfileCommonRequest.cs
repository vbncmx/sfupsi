﻿namespace SfuPsi_Application.Models.PersonGrid
{
    public class UpdateProfileCommonRequest
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Languages { get; set; }
        public string Dob { get; set; }
    }
}