﻿using System.ComponentModel.DataAnnotations;
using SfuPsi_DomainClasses;

namespace SfuPsi_Application.Models.SemesterGrid
{
    public class SemesterVm
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public static SemesterVm FromEntity(Semester semester)
        {
            return new SemesterVm
            {
                Id = semester.Id,
                Title = semester.Title
            };
        }

        public Semester Apply(Semester semester)
        {
            semester.Title = Title;
            return semester;
        }
    }
}