﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Models
{
    public class EmailGridVm
    {
        public const int RoleFilterAllValue = 0;

        public static async Task<EmailGridVm> FromContext(SfuPsiContext context)
        {
            var roles = await context.Roles.ToArrayAsync();
            var emailGridVm = new EmailGridVm
            {
                RoleFilterList = roles.Select(r => new SelectListItem
                {
                    Text = r.RoleTitle,
                    Value = r.RoleId.ToString()
                })
                .Concat(new[] { new SelectListItem { Text = "All", Value = $"{RoleFilterAllValue}" } })
                .ToArray()
            };
            return emailGridVm;
        }

        public SelectListItem[] RoleFilterList { get; set; }
    }
}