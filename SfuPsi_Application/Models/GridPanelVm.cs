﻿namespace SfuPsi_Application.Models
{
    public class GridPanelVm
    {
        public GridPanelVm(string gridId, string addButtonText)
        {
            AddButtonText = addButtonText;
            GridId = gridId;
        }

        public string AddButtonText { get; private set; }
        public string GridId { get; private set; }
    }
}