﻿using System;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Kendo.Mvc.UI.Fluent;
using SfuPsi_Application.Identity;
using SfuPsi_Application.Models;
using SfuPsi_Application.Models.Common;
using DateTime = System.DateTime;

namespace SfuPsi_Application
{
    public static class MarkupExtensions
    {
        public static GridBoundColumnBuilder<TModel> BoundAsMultilineText<TModel>(
            this GridColumnFactory<TModel> gridColumnFactory, Expression<Func<TModel, string>> expression)
             where TModel : class
        {
            var gridBoundColumnBuilder = gridColumnFactory.Bound(expression);
            return gridBoundColumnBuilder.EditorTemplateName("MultilineText");
        }

        public static GridBoundColumnBuilder<TModel> ForeignKeyAutocomplete<TModel, TKey>(
            this GridColumnFactory<TModel> gridColumnFactory, Expression<Func<TModel, TKey>> expression, SelectList data)
            where TModel : class
        {
            return gridColumnFactory.ForeignKey(expression, data)
                .EditorTemplateName("GridForeignKeyAutocomplete");
        }

        public static GridBoundColumnBuilder<TModel> BoundAsDate<TModel>(
            this GridColumnFactory<TModel> gridColumnFactory, Expression<Func<TModel, DateTime>> expression)
             where TModel : class
        {
            var gridBoundColumnBuilder = gridColumnFactory.Bound(expression);
            return gridBoundColumnBuilder.Format(Formats.GridCommonDateFormat)
                .EditorTemplateName("Date").Filterable(c => c.Cell(cell => cell.Template("formatDateElement")));
        }

        public static GridBoundColumnBuilder<TModel> BoundAsDate<TModel>(
            this GridColumnFactory<TModel> gridColumnFactory, Expression<Func<TModel, DateTime?>> expression)
             where TModel : class
        {
            var gridBoundColumnBuilder = gridColumnFactory.Bound(expression);
            return gridBoundColumnBuilder.Format(Formats.GridCommonDateFormat)
                .EditorTemplateName("Date").Filterable(c => c.Cell(cell => cell.Template("formatDateElement")));
        }

        public static GridBoundColumnBuilder<TModel> BoundAsDateTime<TModel>(
            this GridColumnFactory<TModel> gridColumnFactory, Expression<Func<TModel, DateTime>> expression)
             where TModel : class
        {
            var gridBoundColumnBuilder = gridColumnFactory.Bound(expression);
            return gridBoundColumnBuilder.Format(Formats.GridDateTime_dd_MM_yyyy_hh_mm).EditorTemplateName("DateTime");
        }

        public static GridBoundColumnBuilder<TModel> BoundAsTime<TModel>(
            this GridColumnFactory<TModel> gridColumnFactory, Expression<Func<TModel, TimeVm>> expression, string propertyName)
             where TModel : class
        {
            var gridBoundColumnBuilder = gridColumnFactory.Bound(expression);
            return gridBoundColumnBuilder
                .ClientTemplate(Formats.TimePickerTemplate(propertyName))
                .EditorTemplateName("TimePickerEditor");
        }

        public static GridBoundColumnBuilder<TModel> BoundAsBoolean<TModel>(
            this GridColumnFactory<TModel> gridColumnFactory, Expression<Func<TModel, bool>> expression)
             where TModel : class
        {
            var gridBoundColumnBuilder = gridColumnFactory.Bound(expression);
            var expressionHelper = new ExpressionHelper<TModel, bool>();
            var propertyName = expressionHelper[expression];
            return gridBoundColumnBuilder
                .ClientTemplate($"<input type='checkbox' #= {propertyName} ? checked='checked' :'' # />");
        }

        public static bool IsAdmin(this IPrincipal user)
        {
            return user.IsInRole(AppRole.AdminRoleName);
        }

        public static IHtmlString KendoDateTemplate(this HtmlHelper<dynamic> html, string fieldName)
        {
            return new HtmlString($"#= kendo.toString({fieldName}), \"{Formats.CommonDateFormat}\") #");
        }

        public static GridBuilder<TModel> KeepLayout<TModel>(this GridBuilder<TModel> gridBuilder, HtmlHelper html, string gridId) where TModel : class
        {
            var loadScript = "$(document).ready(function () { LoadGridLayout('" + gridId + "'); });";
            html.Assets().InlineScripts.Add(loadScript);
            const string saveHandler = "SaveGridLayout";
            return gridBuilder.ColumnMenu().Events(e => e
                .ColumnHide(saveHandler)
                .ColumnShow(saveHandler)
                .ColumnReorder(saveHandler)
                .ColumnResize(saveHandler)
                .DataBinding(saveHandler)); // to preserve page size
        }

        public static MvcHtmlString GridPanel(this HtmlHelper html, string gridId, string addButtonText)
        {
            return html.Partial("GridPanel", new GridPanelVm(gridId, addButtonText));
        }

        public static string ValueTemplate(this HtmlHelper html, string fieldName)
        {
            return "#if(" + fieldName + "==null) {##=''##}else{##=" + fieldName + "##}#";
        }

        public static HtmlString RawValueTemplate(this HtmlHelper html, string fieldName)
        {
            return new HtmlString(html.ValueTemplate(fieldName));
        }
    }
}