﻿using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class ModuleGridController : Controller
    {
        public ActionResult Get()
        {
            // populate programs for editing in a drop down
            var context = new SfuPsiContext();
            var programs = context.Programs.ToArray().Select(p => new ProgramVm(p));
            var model = new ModuleGridVm
            {
                Programs = new SelectList(programs, nameof(ProgramVm.ProgramId),
                nameof(ProgramVm.ProgramTitle))
            };
            return View("ModuleGrid", model);
        }

        public ActionResult ReadModules([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var modules = context.Modules.ToArray().Select(m => new ModuleViewModel(m)).ToArray();
            var result = modules.ToDataSourceResult(request);
            return Json(result);
        }

        public ActionResult ReadSubmodules(int moduleId, [DataSourceRequest] DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var submodules = context.SubModules
                .Where(s => s.ModuleId == moduleId).ToArray()
                .Select(s => new SubModuleViewModel(s));
            return Json(submodules.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateModule([DataSourceRequest] DataSourceRequest request, ModuleViewModel moduleVm)
        {
            if (moduleVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var module = context.Modules.Find(moduleVm.ModuleId);
                //
                module.ModuleTitle = moduleVm.ModuleTitle;
                module.ProgramId = moduleVm.ProgramId;
                //
                context.SaveChanges();
            }
            return Json(new[] { moduleVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyModule([DataSourceRequest] DataSourceRequest request, ModuleViewModel moduleVm)
        {
            if (moduleVm != null)
            {
                var context = new SfuPsiContext();
                var module = context.Modules.Find(moduleVm.ModuleId);
                if (module != null)
                {
                    context.Modules.Remove(module);
                    context.SaveChanges();
                }
            }
            return Json(new[] { moduleVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateModule([DataSourceRequest] DataSourceRequest request, ModuleViewModel moduleVm)
        {
            if (moduleVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var isDuplicate =
                    context.Modules.Any(i => i.ProgramId == moduleVm.ProgramId && i.ModuleTitle == moduleVm.ModuleTitle);
                if (!isDuplicate) // hack to avoid adding duplicate modules
                {
                    var module = moduleVm.ToModule();
                    context.Modules.Add(module);
                    context.SaveChanges();
                    moduleVm.ModuleId = module.ModuleId;
                }
            }
            return Json(new[] { moduleVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateSubModule([DataSourceRequest] DataSourceRequest request, SubModuleViewModel subModuleVm)
        {
            if (subModuleVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var subModule = context.SubModules.Find(subModuleVm.SubModuleId);
                //
                subModule.Title = subModuleVm.SubModuleTitle;
                //
                context.SaveChanges();
                subModuleVm.SubModuleId = subModule.SubModuleId;
            }
            return Json(new[] { subModuleVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroySubModule([DataSourceRequest] DataSourceRequest request, SubModuleViewModel subModuleVm)
        {
            if (subModuleVm != null)
            {
                var context = new SfuPsiContext();
                var subModule = context.SubModules.Find(subModuleVm.SubModuleId);
                if (subModule != null)
                {
                    context.SubModules.Remove(subModule);
                    context.SaveChanges();
                }
            }
            return Json(new[] { subModuleVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateSubModule(int moduleId, [DataSourceRequest] DataSourceRequest request, SubModuleViewModel subModuleVm)
        {
            if (subModuleVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var subModule = context.SubModules
                    .FirstOrDefault(i => i.ModuleId == moduleId && i.Title == subModuleVm.SubModuleTitle);
                if (subModule == null) // hack to avoid adding duplicate submodules (Telerik Grid Issue)
                {
                    subModule = subModuleVm.ToSubModule();
                    subModule.ModuleId = moduleId;
                    context.SubModules.Add(subModule);
                    context.SaveChanges();
                }
            }
            return Json(new[] { subModuleVm }.ToDataSourceResult(request, ModelState));
        }
    }
}