﻿using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Identity;
using SfuPsi_Application.Models;

namespace SfuPsi_Application.Controllers
{
    [Authorize(Roles = AppRole.AdminRoleName)]
    [LogSessionActivity]
    public class UserGridController : Controller
    {
        public ActionResult Get()
        {
            return View("UserGrid");
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            var manager = this.UserManager();
            var result = manager.Users.ToArray().Select(u => new UserVm(u)).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, UserVm userVm)
        {
            if (userVm != null && ModelState.IsValid)
            {
                var manager = this.UserManager();
                var user = manager.Users.FirstOrDefault(u => u.Id == userVm.Id);
                //
                user.Email = userVm.Email;
                if (user.Id != AppUser.AdminId)
                    user.UserName = userVm.UserName;
                //
                manager.Update(user);
            }
            return Json(new[] { userVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, UserVm userVm)
        {
            if (userVm != null)
            {
                var manager = this.UserManager();
                if (userVm.Id != AppUser.AdminId)
                {
                    var user = manager.FindById(userVm.Id);
                    manager.Delete(user);
                }
            }
            return Json(new[] { userVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, UserVm userVm)
        {
            if (userVm != null && ModelState.IsValid)
            {
                var manager = this.UserManager();
                var user = new AppUser
                {
                    Email = userVm.Email,
                    UserName = userVm.UserName
                };
                var password = $"iam{userVm.UserName}";
                manager.Create(user, password);
            }
            return Json(new[] { userVm }.ToDataSourceResult(request, ModelState));
        }
    }
}