﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SfuPsi_Application.Models.Reporting;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    public class ReportingController : Controller
    {
        // GET: Reporting
        public ActionResult PersonListReport(string reportTitle, string personIdsString)
        {
            var context = new SfuPsiContext();
            var personIds = personIdsString.Split(',').Select(int.Parse).ToArray();
            var model = PlrModel.Construct(personIds, reportTitle, context);

            return View(model);
        }
    }
}