﻿using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class ProgramGridController : Controller
    {
        public ActionResult Get()
        {
            return View("ProgramGrid");
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var result = context.Programs.ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Program updatedProgram)
        {
            if (updatedProgram != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var program = context.Programs.Find(updatedProgram.ProgramId);
                //
                program.Title = updatedProgram.Title;
                //
                context.SaveChanges();
            }
            return Json(new[] { updatedProgram }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, Program program)
        {
            if (program != null)
            {
                var context = new SfuPsiContext();
                program = context.Programs.Find(program.ProgramId);
                if (program != null)
                {
                    context.Programs.Remove(program);
                    context.SaveChanges();
                }
            }
            return Json(new[] { program }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Program program)
        {
            if (program != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                context.Programs.Add(program);
                context.SaveChanges();
            }
            return Json(new[] { program }.ToDataSourceResult(request, ModelState));
        }
    }
}