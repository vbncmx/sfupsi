﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models.ClassTemplateGrid;
using SfuPsi_Application.Models.EventGrid;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class EventGridController : Controller
    {
        public async Task<ActionResult> Get()
        {
            var context = new SfuPsiContext();
            var model = await EventGridVm.Construct(context);
            return View("EventGrid", model);
        }

        public async Task<ActionResult> Read([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var events = await context.Events.ToArrayAsync();
            var result = events.Select(EventVm.Construct).ToDataSourceResult(request);
            return Json(result);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Update([DataSourceRequest] DataSourceRequest request, EventVm eventVm)
        {
            DtHelper.FixModelDateTimeFields(eventVm, ModelState);
            var context = new SfuPsiContext();
            var @event = await context.Events.FindAsync(eventVm.EventId);
            eventVm.Apply(@event);
            await context.SaveChangesAsync();
            return Json(new[] { eventVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Destroy([DataSourceRequest] DataSourceRequest request, EventVm eventVm)
        {
            if (eventVm != null)
            {
                var context = new SfuPsiContext();
                var @event = await context.Events.FindAsync(eventVm.EventId);
                if (@event != null)
                {
                    context.Events.Remove(@event);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { eventVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Create([DataSourceRequest] DataSourceRequest request, EventVm eventVm)
        {
            DtHelper.FixModelDateTimeFields(eventVm, ModelState);
            var context = new SfuPsiContext();
            var @event = eventVm.Apply(new Event());
            context.Events.Add(@event);
            await context.SaveChangesAsync();
            eventVm.EventId = @event.Id;
            return Json(new[] { eventVm }.ToDataSourceResult(request, ModelState));
        }

        public async Task<ActionResult> ReadParticipations([DataSourceRequest] DataSourceRequest request, int eventId)
        {
            var context = new SfuPsiContext();
            var participations = await context.EventParticipations.Where(p => p.EventId == eventId).ToArrayAsync();
            var result = participations.Select(EventParticipationVm.Construct).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> UpdateParticipation([DataSourceRequest] DataSourceRequest request, EventParticipationVm participationVm)
        {
            if (participationVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var participation = await context.EventParticipations.FindAsync(participationVm.Id);
                participationVm.Apply(participation);
                //
                await context.SaveChangesAsync();
            }
            return Json(new[] { participationVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DestroyParticipation([DataSourceRequest] DataSourceRequest request, EventParticipationVm participationVm)
        {
            if (participationVm != null)
            {
                var context = new SfuPsiContext();
                var participation = await context.EventParticipations.FindAsync(participationVm.Id);
                if (participation != null)
                {
                    context.EventParticipations.Remove(participation);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { participationVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreateParticipation(int eventId, [DataSourceRequest] DataSourceRequest request, EventParticipationVm participationVm)
        {
            if (participationVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var isThereDuplicate = await context.EventParticipations
                    .AnyAsync(p => p.PersonId == participationVm.PersonId && p.EventId == eventId);

                if (!isThereDuplicate)
                {
                    var participation = participationVm.Apply(new EventParticipation { EventId = eventId });
                    context.EventParticipations.Add(participation);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { participationVm }.ToDataSourceResult(request, ModelState));
        }
    }
}