﻿using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models;

namespace SfuPsi_Application.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        [LogSessionActivity]
        public ActionResult Login(string returnUrl)
        {
            var login = new LoginViewModel
            {
                ReturnUrl = returnUrl
            };
            return View(login);
        }

        [HttpPost]
        [LogSessionActivity]
        public ActionResult Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                var authManager = HttpContext.GetOwinContext().Authentication;

                var user = this.UserManager().Find(login.UserName, login.Password);
                if (user != null)
                {
                    var ident = this.UserManager().CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                    authManager.SignIn(new AuthenticationProperties { IsPersistent = false }, ident);
                    return Redirect(login.ReturnUrl ?? Url.Action("Index", "Home"));
                }
            }
            ModelState.AddModelError("", "Invalid username or password");
            return View(login);
        }

        [HttpGet]
        [LogSessionActivity]
        public ActionResult Logout()
        {
            var autheticationManager = HttpContext.GetOwinContext().Authentication;
            autheticationManager.SignOut();
            return RedirectToAction("Login", "Home");
        }

        [Authorize]
        [HttpGet]
        [LogSessionActivity]
        public ActionResult Account()
        {
            var userId = User.Identity.GetUserId();
            var user = this.UserManager().Users.FirstOrDefault(u => u.Id == userId);
            var accountVm = new AccountViewModel(user);
            return View(accountVm);
        }

        [Authorize]
        [HttpPost]
        [LogSessionActivity]
        public ActionResult Account(AccountViewModel accountVm)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var manager = this.UserManager();
                var user = manager.Users.FirstOrDefault(u => u.Id == userId);
                user.Email = accountVm.Email;
                manager.Update(user);
                return View(accountVm);
            }
            ModelState.AddModelError("", "Errors occured");
            return View(accountVm);
        }

        [Authorize]
        [HttpGet]
        [LogSessionActivity]
        public ActionResult ChangePassword()
        {
            var model = new ChangePasswordViewModel();
            return View(model);
        }

        [Authorize]
        [HttpPost]
        [LogSessionActivity]
        public ActionResult ChangePassword(ChangePasswordViewModel passwordVm)
        {
            passwordVm.Validate = true;
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var manager = this.UserManager();
                var user = manager.Users.FirstOrDefault(u => u.Id == userId);
                //
                if (!manager.CheckPassword(user, passwordVm.CurrentPassword))
                {
                    ModelState.AddModelError(nameof(ChangePasswordViewModel.CurrentPassword), "Old password is wrong.");
                    return View(passwordVm);
                }
                //
                var changePasswordResult = manager.ChangePassword(userId, passwordVm.CurrentPassword, passwordVm.NewPassword);
                Debug.WriteLine(changePasswordResult.Succeeded);
                return RedirectToAction("Account", "Home");
            }
            return View(passwordVm);
        }
    }
}
