﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class ClassGridController : Controller
    {
        public async Task<ActionResult> Get()
        {
            var context = new SfuPsiContext();
            var model = await ClassGridVm.FromContext(context);
            return View("ClassGrid", model);
        }

        #region Classes
        
        public ActionResult ReadClasses([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var result = context.Classes.ToArray().Select(c => new ClassVm(c)).ToDataSourceResult(request);
            return Json(result);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateClass([DataSourceRequest] DataSourceRequest request, ClassVm classVm)
        {
            if (classVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var classEntity = context.Classes.Find(classVm.ClassId);
                //
                classVm.Apply(classEntity);
                //
                context.SaveChanges();
            }
            return Json(new[] {classVm}.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyClass([DataSourceRequest] DataSourceRequest request, ClassVm classVm)
        {
            if (classVm != null)
            {
                var context = new SfuPsiContext();
                var classEntity = context.Classes.Find(classVm.ClassId);
                if (classEntity != null)
                {
                    context.Classes.Remove(classEntity);
                    context.SaveChanges();
                }
            }
            return Json(new[] { classVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateClass([DataSourceRequest] DataSourceRequest request, ClassVm classVm)
        {
            if (classVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var classEntity = new Class();
                classVm.Apply(classEntity);
                context.Classes.Add(classEntity);
                context.SaveChanges();
            }
            return Json(new[] { classVm }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Class Occurances

        public ActionResult ReadClassOccurances(int classId, [DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var result =
                context.ClassOccurances.Where(c => c.ClassId == classId)
                    .ToArray()
                    .Select(o => new ClassOccuranceVm(o))
                    .ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateClassOccurance([DataSourceRequest] DataSourceRequest request, ClassOccuranceVm classOccuranceVm)
        {
            if (classOccuranceVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var classOccurance = context.ClassOccurances.Find(classOccuranceVm.ClassOccuranceId);
                //
                classOccuranceVm.Apply(classOccurance);
                context.SaveChanges();
                //
                context.SaveChanges();
            }
            return Json(new[] { classOccuranceVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyClassOccurance([DataSourceRequest] DataSourceRequest request, ClassOccuranceVm classOccuranceVm)
        {
            if (classOccuranceVm != null)
            {
                var context = new SfuPsiContext();
                var classOccurance = context.ClassOccurances.Find(classOccuranceVm.ClassOccuranceId);
                if (classOccurance != null)
                {
                    context.ClassOccurances.Remove(classOccurance);
                    context.SaveChanges();
                }
            }
            return Json(new[] { classOccuranceVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateClassOccurance(int classId, [DataSourceRequest] DataSourceRequest request, ClassOccuranceVm classOccuranceVm)
        {
            if (classOccuranceVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var classOccurance = classOccuranceVm.Apply(new ClassOccurance());
                classOccurance.ClassId = classId;
                context.ClassOccurances.Add(classOccurance);
                context.SaveChanges();
            }
            return Json(new[] { classOccuranceVm }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Professors

        public ActionResult ReadProfessors(int classId, [DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var classEntity = context.Classes.Find(classId);
            var professors = classEntity.Professors.ToArray();
            var result = professors.ToArray()
                .Select(p => new ClassProfessorVm(p)).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DestroyProfessor(int classId, [DataSourceRequest] DataSourceRequest request, ClassProfessorVm professorVm)
        {
            var context = new SfuPsiContext();
            var classEntity = context.Classes.Find(classId);
            var professor = classEntity.Professors.FirstOrDefault(p => p.PersonId == professorVm.PersonId);
            if (professor != null)
            {
                classEntity.Professors.Remove(professor);
                context.SaveChanges();
            }
            return Json(new[] { professorVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateProfessor(int classId, [DataSourceRequest] DataSourceRequest request, ClassProfessorVm professorVm)
        {
            var context = new SfuPsiContext();
            var classEntity = context.Classes.Find(classId);
            var isProfessorAlreadyInClass = classEntity.Professors.Any(p => p.PersonId == professorVm.PersonId);
            if (!isProfessorAlreadyInClass)
            {
                var professor = context.Persons.Find(professorVm.PersonId);
                classEntity.Professors.Add(professor);
                context.SaveChanges();
            }
            return Json(new[] { professorVm }.ToDataSourceResult(request, ModelState));
        }

        #endregion
    }
}