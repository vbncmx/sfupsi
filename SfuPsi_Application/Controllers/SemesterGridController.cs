﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models.SemesterGrid;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class SemesterGridController : Controller
    {
        public ActionResult Get()
        {
            return View("SemesterGrid");
        }

        public async Task<ActionResult> Read([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var semesters = await context.Semesters.ToArrayAsync();
            var result = semesters.Select(SemesterVm.FromEntity).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Update([DataSourceRequest] DataSourceRequest request, SemesterVm semesterVm)
        {
            if (semesterVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var semester = context.Semesters.Find(semesterVm.Id);
                //
                semesterVm.Apply(semester);
                //
                await context.SaveChangesAsync();
            }
            return Json(new[] { semesterVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Destroy([DataSourceRequest] DataSourceRequest request, SemesterVm semesterVm)
        {
            if (semesterVm != null)
            {
                var context = new SfuPsiContext();
                var semester = await context.Semesters.FindAsync(semesterVm.Id);
                if (semester != null)
                {
                    context.Semesters.Remove(semester);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { semesterVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Create([DataSourceRequest] DataSourceRequest request, SemesterVm semesterVm)
        {
            if (semesterVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var isThereADuplicate = await context.Semesters.AnyAsync(s => s.Title == semesterVm.Title);
                if (!isThereADuplicate)
                {
                    var semester = semesterVm.Apply(new Semester());
                    context.Semesters.Add(semester);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { semesterVm }.ToDataSourceResult(request, ModelState));
        }
    }
}