﻿using System;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class RoleGridController : Controller
    {
        public ActionResult Get()
        {
            return View("RoleGrid");
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var result = context.Roles.OrderBy(r => r.Order).ToArray().Select(r => new RoleVm(r)).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Role updatedRole)
        {
            if (updatedRole == null)
                return Json(new[] { updatedRole }.ToDataSourceResult(request, ModelState));

            var context = new SfuPsiContext();
            var role = context.Roles.Find(updatedRole.RoleId);

            // prevent updating built in roles (Student, Professor)
            if (Role.BuiltInRoleIds.Contains(updatedRole.RoleId))
            {
                ModelState.AddModelError(Guid.NewGuid().ToString(), $"Can not edit built in role '{role.RoleTitle}'");
                updatedRole = context.Roles.Find(updatedRole.RoleId);
            }

            else if (ModelState.IsValid)
            {
                role.RoleTitle = updatedRole.RoleTitle;
                role.Order = updatedRole.Order;
                //
                context.SaveChanges();
            }
            return Json(new[] { updatedRole }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, Role role)
        {
            Guard.ArgumentIsNotNull(role, nameof(role));

            var context = new SfuPsiContext();
            role = context.Roles.Find(role.RoleId);

            // prevent deleting built in roles
            if (Role.BuiltInRoleIds.Contains(role.RoleId))
            {
                ModelState.AddModelError(Guid.NewGuid().ToString(), $"Can not delete built in role '{role.RoleTitle}'");
                return Json(new[] { role }.ToDataSourceResult(request, ModelState));
            }

            context.Roles.Remove(role);
            context.SaveChanges();

            return Json(new[] { role }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Role role)
        {
            if (role != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                context.Roles.Add(role);
                context.SaveChanges();
            }
            return Json(new[] { role }.ToDataSourceResult(request, ModelState));
        }
    }
}