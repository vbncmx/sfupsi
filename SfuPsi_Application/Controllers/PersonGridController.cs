﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models;
using SfuPsi_Application.Models.PersonGrid;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class PersonGridController : Controller
    {
        public async Task<ActionResult> Get()
        {
            var context = new SfuPsiContext();
            var model = await PersonGridVm.FromContext(context);
            return View("PersonGrid", model);
        }

        #region Person CRUD

        public async Task<ActionResult> ReadPersons(string selectionFilteringMode, int[] selectedPersonIds, [DataSourceRequest]DataSourceRequest request)
        {
            var roleId = Request.Params["roleId"] == null
                ? PersonGridVm.RoleFilterAllValue
                : int.Parse(Request.Params["roleId"]);

            var context = new SfuPsiContext();
            Person[] persons;
            if (roleId == PersonGridVm.RoleFilterAllValue)
            {
                persons = await context.Persons.ToArrayAsync();
            }
            else
            {
                persons = await context.PersonRoles.Where(r => r.RoleId == roleId).Select(r => r.Person).ToArrayAsync();
            }

            if (selectionFilteringMode == "showSelected")
            {
                if (selectedPersonIds == null || selectedPersonIds.Length == 0)
                    persons = new Person[] { };
                else
                    persons = persons.Where(p => selectedPersonIds.Contains(p.PersonId)).ToArray();
            }

            var result = persons.Select(PersonVm.Construct).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> UpdatePerson([DataSourceRequest] DataSourceRequest request, PersonVm personVm)
        {
            DtHelper.FixModelDateTimeFields(personVm, ModelState);
            var context = new SfuPsiContext();
            var person = await context.Persons.FindAsync(personVm.PersonId);
            personVm.Apply(person);
            await context.SaveChangesAsync();
            return Json(new[] { personVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DestroyPerson([DataSourceRequest] DataSourceRequest request, PersonVm personVm)
        {
            if (personVm != null)
            {
                var context = new SfuPsiContext();
                var person = await context.Persons.FindAsync(personVm.PersonId);
                if (person != null)
                {
                    context.Persons.Remove(person);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { personVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreatePerson([DataSourceRequest] DataSourceRequest request, PersonVm personVm)
        {
            DtHelper.FixModelDateTimeFields(personVm, ModelState);
            var context = new SfuPsiContext();
            var person = personVm.Apply(new Person());
            context.Persons.Add(person);
            await context.SaveChangesAsync();
            personVm.PersonId = person.PersonId;
            personVm.PhotoUrl = "/Images/nophoto.jpg";
            return Json(new[] { personVm }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public async Task<ActionResult> UpdatePersonK1(int personId, string note, string date)
        {
            try
            {
                var parsedDate = date.ParseAsDate();
                var context = new SfuPsiContext();
                var person = await context.Persons.FindAsync(personId);
                person.StudentK1 = note;
                person.StudentK1Date = parsedDate;
                await context.SaveChangesAsync();
                return Json(UpdateK1Response.SuccessfulResponse(parsedDate, note));

            }
            catch (Exception exc)
            {
                return Json(UpdateK1Response.ErrorResponse(exc));
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdatePersonK2(int personId, string note, string date)
        {
            try
            {
                var parsedDate = date.ParseAsDate();
                var context = new SfuPsiContext();
                var person = await context.Persons.FindAsync(personId);
                person.StudentK2 = note;
                person.StudentK2Date = parsedDate;
                await context.SaveChangesAsync();
                return Json(UpdateK2Response.SuccessfulResponse(parsedDate, note));

            }
            catch (Exception exc)
            {
                return Json(UpdateK2Response.ErrorResponse(exc));
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdateProfileContact(UpdateContactRequest request)
        {
            try
            {
                var context = new SfuPsiContext();
                var person = await context.Persons.FindAsync(request.PersonId);
                request.Apply(person);
                await context.SaveChangesAsync();
                return Json(UpdateContactResponse.SuccessfullResponse(request));
            }
            catch (Exception exc)
            {
                return Json(UpdateContactResponse.ErrorResponse(exc));
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdateProfileCommon(UpdateProfileCommonRequest request)
        {
            var context = new SfuPsiContext();
            var person = await context.Persons.FindAsync(request.PersonId);
            person.SetName(request.Name);
            person.DateOfBirth = request.Dob.ParseAsDate();
            person.Title = request.Title;
            person.Languages = request.Languages;
            await context.SaveChangesAsync();
            var response = new
            {
                Success = true,
                Name = request.Name,
                Dob = person.DateOfBirth,
                Title = request.Title,
                Languages = request.Languages,
                PhotoUrl = person.PhotoUrl()
            };
            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateProfileNote(int personId, string note)
        {
            var context = new SfuPsiContext();
            var person = await context.Persons.FindAsync(personId);
            person.Note = note;
            await context.SaveChangesAsync();
            var response = new
            {
                Success = true,
                Message = "Updated",
                Note = note,
                PhotoUrl = person.PhotoUrl()
            };
            return Json(response);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateProfilePhoto(int personId)
        {
            try
            {
                var file = Request.Files[0];
                var webImage = new WebImage(file.InputStream);
                if (webImage.Width > 200 || webImage.Height > 200)
                    webImage.Resize(200, 200);
                var uploadsFolder = HttpContext.Server.MapPath("~/Images/Uploads");
                if (!Directory.Exists(uploadsFolder))
                    Directory.CreateDirectory(uploadsFolder);

                var savePath = Path.Combine(uploadsFolder, $"PersonPhoto_{personId}.jpg");
                webImage.Save(savePath, "jpg");
                var photoUrl = $"/Images/Uploads/PersonPhoto_{personId}.jpg?{DateTime.Now}";

                var context = new SfuPsiContext();
                var person = await context.Persons.FindAsync(personId);
                person.IsPhotoLoaded = true;
                await context.SaveChangesAsync();

                return Json(UploadPhotoResponse.SuccessfullResponse(photoUrl));
            }
            catch (Exception exc)
            {
                return Json(UploadPhotoResponse.ErrorResponse(exc));
            }
        }

        [HttpPost]
        public async Task<ActionResult> RemoveProfilePhoto(int personId)
        {
            try
            {
                var context = new SfuPsiContext();
                var person = await context.Persons.FindAsync(personId);
                person.IsPhotoLoaded = false;
                await context.SaveChangesAsync();
                var response = RemovePhotoResponse.SuccessfullResponse();
                var result = Json(response);
                return result;
            }
            catch (Exception exc)
            {
                var response = RemovePhotoResponse.ErrorResponse(exc);
                return Json(response);
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdateStudentData(UpdateStudentDataRequest request, int personId)
        {
            try
            {
                var context = new SfuPsiContext();
                var person = await context.Persons.FindAsync(personId);
                request.Apply(person);
                await context.SaveChangesAsync();
                var response = UpdateStudentDataResponse.Successful(request);
                return Json(response);
            }
            catch (Exception exc)
            {
                var response = UpdateStudentDataResponse.Failed(exc);
                return Json(response);
            }
        }

        #endregion

        #region PersonRole CRUD

        public async Task<ActionResult> ReadPersonRoles([DataSourceRequest] DataSourceRequest request, int personId)
        {
            var context = new SfuPsiContext();
            var roles = await context.PersonRoles.Where(r => r.PersonId == personId).ToArrayAsync();
            var result = roles.Select(r => new PersonRoleVm(r)).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> UpdatePersonRole([DataSourceRequest] DataSourceRequest request, PersonRoleVm personRoleVm)
        {
            if (personRoleVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var personRole = await context.PersonRoles.FindAsync(personRoleVm.PersonRoleId);
                //
                personRole.RoleId = personRoleVm.RoleId;
                personRole.StartDate = personRoleVm.StartDate;
                personRole.EndDate = personRoleVm.EndDate;
                //
                await context.SaveChangesAsync();
            }
            return Json(new[] { personRoleVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DestroyPersonRole([DataSourceRequest] DataSourceRequest request, PersonRoleVm personRoleVm)
        {
            if (personRoleVm != null)
            {
                var context = new SfuPsiContext();
                var personRole = await context.PersonRoles.FindAsync(personRoleVm.PersonRoleId);
                if (personRole != null)
                {
                    context.PersonRoles.Remove(personRole);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { personRoleVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreatePersonRole(int personId, [DataSourceRequest] DataSourceRequest request, PersonRoleVm personRoleVm)
        {
            if (personRoleVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var isThereDuplicate =
                    await context.PersonRoles.AnyAsync(r => r.PersonId == personId && r.RoleId == personRoleVm.RoleId);
                if (!isThereDuplicate)
                {
                    var personRole = new PersonRole
                    {
                        PersonId = personId,
                        EndDate = personRoleVm.EndDate,
                        StartDate = personRoleVm.StartDate,
                        RoleId = personRoleVm.RoleId
                    };
                    context.PersonRoles.Add(personRole);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { personRoleVm }.ToDataSourceResult(request, ModelState));
        }

        #endregion
        
    }
}