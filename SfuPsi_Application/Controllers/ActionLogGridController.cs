﻿using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.Identity;
using SfuPsi_Application.Models;

namespace SfuPsi_Application.Controllers
{
    [Authorize(Roles = AppRole.AdminRoleName)]
    public class ActionLogGridController : Controller
    {
        public ActionResult Get()
        {
            return View("ActionLogGrid");
        }

        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            var context = new IdentityContext();
            var result = context.ActionLogRecords.AsEnumerable()
                    .Select(ActionLogRecordVm.FromEntity)
                    .ToDataSourceResult(request);
            return Json(result);
        }
    }
}