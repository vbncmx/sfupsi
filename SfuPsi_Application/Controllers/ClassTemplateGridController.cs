﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models.ClassTemplateGrid;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class ClassTemplateGridController : Controller
    {
        public async Task<ActionResult> Get()
        {
            var context = new SfuPsiContext();
            var model = await ClassTemplateGridVm.Construct(context);
            return View("ClassTemplateGrid", model);
        }

        public async Task<ActionResult> Read([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var classTemplates = await context.ClassTemplates.ToArrayAsync();
            var result = classTemplates.Select(ClassTemplateVm.Construct).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Update([DataSourceRequest] DataSourceRequest request, ClassTemplateVm classTemplateVm)
        {
            if (classTemplateVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var classTemplate = await context.ClassTemplates.FindAsync(classTemplateVm.ClassTemplateId);
                classTemplateVm.Apply(classTemplate);
                await context.SaveChangesAsync();
            }
            return Json(new[] { classTemplateVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Destroy([DataSourceRequest] DataSourceRequest request, ClassTemplateVm classTemplateVm)
        {
            if (classTemplateVm != null)
            {
                var context = new SfuPsiContext();
                var classTemplate = await context.ClassTemplates.FindAsync(classTemplateVm.ClassTemplateId);
                if (classTemplate != null)
                {
                    context.ClassTemplates.Remove(classTemplate);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { classTemplateVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Create([DataSourceRequest] DataSourceRequest request, ClassTemplateVm classTemplateVm)
        {
            if (classTemplateVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var classTemplate = classTemplateVm.Apply(new ClassTemplate());
                context.ClassTemplates.Add(classTemplate);
                await context.SaveChangesAsync();
                classTemplateVm.ClassTemplateId = classTemplate.ClassTemplateId;
            }
            return Json(new[] { classTemplateVm }.ToDataSourceResult(request, ModelState));
        }

        public async Task<ActionResult> ReadParticipations([DataSourceRequest] DataSourceRequest request, int classTemplateId)
        {
            var context = new SfuPsiContext();
            var participations = await context.ClassParticipations
                .Where(p => p.ClassTemplateId == classTemplateId).ToArrayAsync();
            var result = participations.Select(ClassParticipationVm.Construct).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> UpdateParticipation([DataSourceRequest] DataSourceRequest request, ClassParticipationVm participationVm)
        {
            if (participationVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var participation = await context.ClassParticipations.FindAsync(participationVm.Id);
                participationVm.Apply(participation);
                //
                await context.SaveChangesAsync();
            }
            return Json(new[] { participationVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> DestroyParticipation([DataSourceRequest] DataSourceRequest request, ClassParticipationVm participationVm)
        {
            if (participationVm != null)
            {
                var context = new SfuPsiContext();
                var participation = await context.ClassParticipations.FindAsync(participationVm.Id);
                if (participation != null)
                {
                    context.ClassParticipations.Remove(participation);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { participationVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreateParticipation(int classTemplateId, [DataSourceRequest] DataSourceRequest request, ClassParticipationVm participationVm)
        {
            if (participationVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var isThereDuplicate = await context.ClassParticipations
                    .AnyAsync(p => p.PersonId == participationVm.PersonId && p.ClassTemplateId == classTemplateId && p.SemesterId == participationVm.SemesterId);

                if (!isThereDuplicate)
                {
                    var participation = participationVm.Apply(new ClassParticipation {ClassTemplateId = classTemplateId});
                    context.ClassParticipations.Add(participation);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { participationVm }.ToDataSourceResult(request, ModelState));
        }
    }
}