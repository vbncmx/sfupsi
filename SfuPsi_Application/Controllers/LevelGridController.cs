﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SfuPsi_Application.ActionLog;
using SfuPsi_Application.Models;
using SfuPsi_Application.Models.LevelGrid;
using SfuPsi_DomainClasses;
using SfuPsi_DomainData;

namespace SfuPsi_Application.Controllers
{
    [Authorize]
    [LogSessionActivity]
    public class LevelGridController : Controller
    {
        public async Task<ActionResult> Get()
        {
            var context = new SfuPsiContext();
            var model = await LevelGridVm.FromContext(context);
            return View("LevelGrid", model);
        }

        public async Task<ActionResult> Read([DataSourceRequest]DataSourceRequest request)
        {
            var context = new SfuPsiContext();
            var levels = await context.Levels.ToArrayAsync();
            var result = levels.Select(LevelVm.FromEntity).ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Update([DataSourceRequest] DataSourceRequest request, LevelVm levelVm)
        {
            if (levelVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var level = context.Levels.Find(levelVm.LevelId);
                //
                levelVm.Apply(level);
                //
                await context.SaveChangesAsync();
            }
            return Json(new[] { levelVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Destroy([DataSourceRequest] DataSourceRequest request, LevelVm levelVm)
        {
            if (levelVm != null)
            {
                var context = new SfuPsiContext();
                var level = await context.Levels.FindAsync(levelVm.LevelId);
                if (level != null)
                {
                    context.Levels.Remove(level);
                    await context.SaveChangesAsync();
                }
            }
            return Json(new[] { levelVm }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Create([DataSourceRequest] DataSourceRequest request, LevelVm levelVm)
        {
            if (levelVm != null && ModelState.IsValid)
            {
                var context = new SfuPsiContext();
                var level = levelVm.Apply(new Level());
                context.Levels.Add(level);
                await context.SaveChangesAsync();
            }
            return Json(new[] { levelVm }.ToDataSourceResult(request, ModelState));
        }
    }
}