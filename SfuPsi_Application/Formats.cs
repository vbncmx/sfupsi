﻿using System;
using static System.String;

namespace SfuPsi_Application
{
    public class Formats
    {
        public const string CommonDateFormat = "dd.MM.yyyy";

        public const string GridDateTime_dd_MM_yyyy_hh_mm = "{0:dd.MM.yyyy hh:mm}";
        
        public const string DateTimePickerFormat = "dd.MM.yyyy hh:mm";
        public static string GridCommonDateFormat => "{0:" + CommonDateFormat + "}";

        public static string TimePickerTemplate(string timeFieldName)
        {
            var template = "\\#= kendo.toString([FN].Hours, \"00\") \\#:\\#= kendo.toString([FN].Minutes, \"00\") \\#";
            return template.Replace("[FN]", timeFieldName);
        }
    }
}