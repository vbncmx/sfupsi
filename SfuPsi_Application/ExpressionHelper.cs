﻿using System;
using System.Linq.Expressions;


namespace SfuPsi_Application
{
    public class ExpressionHelper<TObject, TProperty>
    {
        public string this[Expression<Func<TObject, TProperty>> expression] => GetPropertyName(expression);

        public string GetPropertyName(Expression<Func<TObject, TProperty>> expression)
        {
            var body = expression.Body as MemberExpression ??
                       ((UnaryExpression)expression.Body).Operand as MemberExpression;
            return body?.Member.Name;
        }
    }
}