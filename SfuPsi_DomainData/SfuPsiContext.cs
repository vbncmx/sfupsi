﻿using System.Data.Entity;
using SfuPsi_DomainClasses;

namespace SfuPsi_DomainData
{
    public class SfuPsiContext : DbContext
    {
        public SfuPsiContext() : base("name=SfuPsi")
        {
        }

        public DbSet<Class> Classes { get; set; }
        public DbSet<ClassOccurance> ClassOccurances { get; set; }
        public DbSet<ClassTemplate> ClassTemplates { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PersonRole> PersonRoles { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<SubModule> SubModules { get; set; }
        public DbSet<ClassParticipation> ClassParticipations { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventParticipation> EventParticipations { get; set; }
        public DbSet<Semester> Semesters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder
                .Entity<Class>()
                .HasMany(c => c.Professors)
                .WithMany(p => p.TeachedClasses)
                .Map(i => i.ToTable("ClassProfessors"));
        }

        public static void Configure()
        {
            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<SfuPsiContext, Migrations.Configuration>());
            var applicationContext = new SfuPsiContext();
            applicationContext.Database.Initialize(true);
            applicationContext.CheckDefaultRoles();
        }
    }
}