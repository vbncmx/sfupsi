namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Order_To_Role : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Roles", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Roles", "Order");
        }
    }
}
