namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Make_Level_ProgramId_Nullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Levels", "ProgramId", "dbo.Programs");
            DropIndex("dbo.Levels", new[] { "ProgramId" });
            AlterColumn("dbo.Levels", "ProgramId", c => c.Int());
            CreateIndex("dbo.Levels", "ProgramId");
            AddForeignKey("dbo.Levels", "ProgramId", "dbo.Programs", "ProgramId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Levels", "ProgramId", "dbo.Programs");
            DropIndex("dbo.Levels", new[] { "ProgramId" });
            AlterColumn("dbo.Levels", "ProgramId", c => c.Int(nullable: false));
            CreateIndex("dbo.Levels", "ProgramId");
            AddForeignKey("dbo.Levels", "ProgramId", "dbo.Programs", "ProgramId", cascadeDelete: true);
        }
    }
}
