namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_SudentK1Date_StudentK2Date_To_Person : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "StudentK1Date", c => c.DateTime());
            AddColumn("dbo.People", "StudentK2Date", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "StudentK2Date");
            DropColumn("dbo.People", "StudentK1Date");
        }
    }
}
