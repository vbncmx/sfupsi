namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Semester_Instead_Of_Date_To_ClassParticipation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Semesters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ClassParticipations", "SemesterId", c => c.Int());
            CreateIndex("dbo.ClassParticipations", "SemesterId");
            AddForeignKey("dbo.ClassParticipations", "SemesterId", "dbo.Semesters", "Id");
            DropColumn("dbo.ClassParticipations", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ClassParticipations", "Date", c => c.DateTime());
            DropForeignKey("dbo.ClassParticipations", "SemesterId", "dbo.Semesters");
            DropIndex("dbo.ClassParticipations", new[] { "SemesterId" });
            DropColumn("dbo.ClassParticipations", "SemesterId");
            DropTable("dbo.Semesters");
        }
    }
}
