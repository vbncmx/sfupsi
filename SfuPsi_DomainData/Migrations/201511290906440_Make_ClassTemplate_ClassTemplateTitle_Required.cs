namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Make_ClassTemplate_ClassTemplateTitle_Required : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ClassTemplates", "ClassTemplateTitle", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ClassTemplates", "ClassTemplateTitle", c => c.String());
        }
    }
}
