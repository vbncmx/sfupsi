namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Make_Level_LevelTitle_Required : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Levels", "LevelTitle", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Levels", "LevelTitle", c => c.String());
        }
    }
}
