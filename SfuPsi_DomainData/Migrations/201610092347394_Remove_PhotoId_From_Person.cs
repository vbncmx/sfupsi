namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_PhotoId_From_Person : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.People", "PhotoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.People", "PhotoId", c => c.String());
        }
    }
}
