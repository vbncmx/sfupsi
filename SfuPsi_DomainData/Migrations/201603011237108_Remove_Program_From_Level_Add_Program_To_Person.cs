namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Program_From_Level_Add_Program_To_Person : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Levels", "ProgramId", "dbo.Programs");
            DropIndex("dbo.Levels", new[] { "ProgramId" });
            AddColumn("dbo.People", "ProgramId", c => c.Int());
            CreateIndex("dbo.People", "ProgramId");
            AddForeignKey("dbo.People", "ProgramId", "dbo.Programs", "ProgramId");
            DropColumn("dbo.Levels", "ProgramId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Levels", "ProgramId", c => c.Int());
            DropForeignKey("dbo.People", "ProgramId", "dbo.Programs");
            DropIndex("dbo.People", new[] { "ProgramId" });
            DropColumn("dbo.People", "ProgramId");
            CreateIndex("dbo.Levels", "ProgramId");
            AddForeignKey("dbo.Levels", "ProgramId", "dbo.Programs", "ProgramId");
        }
    }
}
