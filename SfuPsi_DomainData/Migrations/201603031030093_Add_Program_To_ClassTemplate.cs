namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Program_To_ClassTemplate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClassTemplates", "ProgramId", c => c.Int());
            CreateIndex("dbo.ClassTemplates", "ProgramId");
            AddForeignKey("dbo.ClassTemplates", "ProgramId", "dbo.Programs", "ProgramId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassTemplates", "ProgramId", "dbo.Programs");
            DropIndex("dbo.ClassTemplates", new[] { "ProgramId" });
            DropColumn("dbo.ClassTemplates", "ProgramId");
        }
    }
}
