using System.Data.Entity.Migrations;

namespace SfuPsi_DomainData.Migrations
{
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Classes",
                c => new
                {
                    ClassId = c.Int(false, true),
                    SemesterId = c.Int(false),
                    LevelId = c.Int(false),
                    Title = c.String(),
                    Description = c.String(),
                    Methodology = c.String(),
                    SuccessCondition = c.String(),
                    Links = c.String(),
                    RecommendedLiterature = c.String(),
                    ProfessorComment = c.String(),
                    ClassTemplateId = c.Int(),
                    SubModuleId = c.Int()
                })
                .PrimaryKey(t => t.ClassId)
                .ForeignKey("dbo.ClassTemplates", t => t.ClassTemplateId)
                .ForeignKey("dbo.Levels", t => t.LevelId, true)
                .ForeignKey("dbo.Semesters", t => t.SemesterId, true)
                .ForeignKey("dbo.SubModules", t => t.SubModuleId)
                .Index(t => t.SemesterId)
                .Index(t => t.LevelId)
                .Index(t => t.ClassTemplateId)
                .Index(t => t.SubModuleId);

            CreateTable(
                "dbo.ClassTemplates",
                c => new
                {
                    ClassTemplateId = c.Int(false, true),
                    ClassTemplateTitle = c.String(),
                    LevelId = c.Int(false),
                    ClassTitle = c.String(),
                    ClassDescription = c.String(),
                    ClassMethodology = c.String(),
                    ClassSuccessCondition = c.String(),
                    ClassLinks = c.String(),
                    ClassRecommendedLiterature = c.String(),
                    ClassProfessorComment = c.String(),
                    ClassSubModuleId = c.Int(false)
                })
                .PrimaryKey(t => t.ClassTemplateId);

            CreateTable(
                "dbo.Levels",
                c => new
                {
                    LevelId = c.Int(false, true),
                    ProgramId = c.Int(false),
                    LevelTitle = c.String()
                })
                .PrimaryKey(t => t.LevelId)
                .ForeignKey("dbo.Programs", t => t.ProgramId, true)
                .Index(t => t.ProgramId);

            CreateTable(
                "dbo.Programs",
                c => new
                {
                    ProgramId = c.Int(false, true),
                    Title = c.String()
                })
                .PrimaryKey(t => t.ProgramId);

            CreateTable(
                "dbo.People",
                c => new
                {
                    PersonId = c.Int(false, true),
                    Name = c.String(),
                    Email = c.String(),
                    DateOfBirth = c.DateTime(),
                    Title = c.String(),
                    PhotoUrl = c.String(),
                    Note = c.String(),
                    PhoneNumber1 = c.String(),
                    PhoneNumber2 = c.String(),
                    Country = c.String(),
                    City = c.String(),
                    Zip = c.String(),
                    Street = c.String(),
                    WebSite = c.String(),
                    StudentAdmissionDate = c.DateTime(),
                    StudentAnalyst = c.String(),
                    StudentSupervision = c.String(),
                    StudentPreExam = c.String(),
                    StudentK1 = c.String(),
                    StudentK2 = c.String()
                })
                .PrimaryKey(t => t.PersonId);

            CreateTable(
                "dbo.Semesters",
                c => new
                {
                    SemesterId = c.Int(false, true),
                    SemesterTitle = c.String()
                })
                .PrimaryKey(t => t.SemesterId);

            CreateTable(
                "dbo.SubModules",
                c => new
                {
                    SubModuleId = c.Int(false, true),
                    ModuleId = c.Int(false),
                    Title = c.String()
                })
                .PrimaryKey(t => t.SubModuleId)
                .ForeignKey("dbo.Modules", t => t.ModuleId, true)
                .Index(t => t.ModuleId);

            CreateTable(
                "dbo.Modules",
                c => new
                {
                    ModuleId = c.Int(false, true),
                    ModuleTitle = c.String(),
                    ProgramId = c.Int(false)
                })
                .PrimaryKey(t => t.ModuleId)
                .ForeignKey("dbo.Programs", t => t.ProgramId, true)
                .Index(t => t.ProgramId);

            CreateTable(
                "dbo.ClassOccurances",
                c => new
                {
                    ClassOccuranceId = c.Int(false, true),
                    ClassId = c.Int(false),
                    Date = c.DateTime(false),
                    StartTime = c.Time(false, 7),
                    Duration = c.Time(false, 7)
                })
                .PrimaryKey(t => t.ClassOccuranceId)
                .ForeignKey("dbo.Classes", t => t.ClassId, true)
                .Index(t => t.ClassId);

            CreateTable(
                "dbo.PersonRoles",
                c => new
                {
                    PersonRoleId = c.Int(false, true),
                    PersonId = c.Int(false),
                    RoleId = c.Int(false),
                    StartDate = c.DateTime(false),
                    EndDate = c.DateTime()
                })
                .PrimaryKey(t => t.PersonRoleId)
                .ForeignKey("dbo.People", t => t.PersonId, true)
                .ForeignKey("dbo.Roles", t => t.RoleId, true)
                .Index(t => t.PersonId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.Roles",
                c => new
                {
                    RoleId = c.Int(false, true),
                    RoleTitle = c.String()
                })
                .PrimaryKey(t => t.RoleId);

            CreateTable(
                "dbo.StudentEnrollments",
                c => new
                {
                    StudentEnrollmentId = c.Int(false, true),
                    PersonId = c.Int(false),
                    LevelId = c.Int(false),
                    SemesterId = c.Int(false)
                })
                .PrimaryKey(t => t.StudentEnrollmentId)
                .ForeignKey("dbo.Levels", t => t.LevelId, true)
                .ForeignKey("dbo.People", t => t.PersonId, true)
                .ForeignKey("dbo.Semesters", t => t.SemesterId, true)
                .Index(t => t.PersonId)
                .Index(t => t.LevelId)
                .Index(t => t.SemesterId);

            CreateTable(
                "dbo.ClassProfessors",
                c => new
                {
                    Class_ClassId = c.Int(false),
                    Person_PersonId = c.Int(false)
                })
                .PrimaryKey(t => new {t.Class_ClassId, t.Person_PersonId})
                .ForeignKey("dbo.Classes", t => t.Class_ClassId, true)
                .ForeignKey("dbo.People", t => t.Person_PersonId, true)
                .Index(t => t.Class_ClassId)
                .Index(t => t.Person_PersonId);
        }

        public override void Down()
        {
            DropForeignKey("dbo.StudentEnrollments", "SemesterId", "dbo.Semesters");
            DropForeignKey("dbo.StudentEnrollments", "PersonId", "dbo.People");
            DropForeignKey("dbo.StudentEnrollments", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.PersonRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.PersonRoles", "PersonId", "dbo.People");
            DropForeignKey("dbo.ClassOccurances", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Classes", "SubModuleId", "dbo.SubModules");
            DropForeignKey("dbo.SubModules", "ModuleId", "dbo.Modules");
            DropForeignKey("dbo.Modules", "ProgramId", "dbo.Programs");
            DropForeignKey("dbo.Classes", "SemesterId", "dbo.Semesters");
            DropForeignKey("dbo.ClassProfessors", "Person_PersonId", "dbo.People");
            DropForeignKey("dbo.ClassProfessors", "Class_ClassId", "dbo.Classes");
            DropForeignKey("dbo.Classes", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.Levels", "ProgramId", "dbo.Programs");
            DropForeignKey("dbo.Classes", "ClassTemplateId", "dbo.ClassTemplates");
            DropIndex("dbo.ClassProfessors", new[] {"Person_PersonId"});
            DropIndex("dbo.ClassProfessors", new[] {"Class_ClassId"});
            DropIndex("dbo.StudentEnrollments", new[] {"SemesterId"});
            DropIndex("dbo.StudentEnrollments", new[] {"LevelId"});
            DropIndex("dbo.StudentEnrollments", new[] {"PersonId"});
            DropIndex("dbo.PersonRoles", new[] {"RoleId"});
            DropIndex("dbo.PersonRoles", new[] {"PersonId"});
            DropIndex("dbo.ClassOccurances", new[] {"ClassId"});
            DropIndex("dbo.Modules", new[] {"ProgramId"});
            DropIndex("dbo.SubModules", new[] {"ModuleId"});
            DropIndex("dbo.Levels", new[] {"ProgramId"});
            DropIndex("dbo.Classes", new[] {"SubModuleId"});
            DropIndex("dbo.Classes", new[] {"ClassTemplateId"});
            DropIndex("dbo.Classes", new[] {"LevelId"});
            DropIndex("dbo.Classes", new[] {"SemesterId"});
            DropTable("dbo.ClassProfessors");
            DropTable("dbo.StudentEnrollments");
            DropTable("dbo.Roles");
            DropTable("dbo.PersonRoles");
            DropTable("dbo.ClassOccurances");
            DropTable("dbo.Modules");
            DropTable("dbo.SubModules");
            DropTable("dbo.Semesters");
            DropTable("dbo.People");
            DropTable("dbo.Programs");
            DropTable("dbo.Levels");
            DropTable("dbo.ClassTemplates");
            DropTable("dbo.Classes");
        }
    }
}