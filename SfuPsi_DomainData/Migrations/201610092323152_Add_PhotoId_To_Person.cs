namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_PhotoId_To_Person : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "PhotoId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "PhotoId");
        }
    }
}
