namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Semester_Everywhere : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Classes", "SemesterId", "dbo.Semesters");
            DropForeignKey("dbo.StudentEnrollments", "LevelId", "dbo.Levels");
            DropForeignKey("dbo.StudentEnrollments", "PersonId", "dbo.People");
            DropForeignKey("dbo.StudentEnrollments", "SemesterId", "dbo.Semesters");
            DropIndex("dbo.Classes", new[] { "SemesterId" });
            DropIndex("dbo.StudentEnrollments", new[] { "PersonId" });
            DropIndex("dbo.StudentEnrollments", new[] { "LevelId" });
            DropIndex("dbo.StudentEnrollments", new[] { "SemesterId" });
            DropColumn("dbo.Classes", "SemesterId");
            DropTable("dbo.Semesters");
            DropTable("dbo.StudentEnrollments");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.StudentEnrollments",
                c => new
                    {
                        StudentEnrollmentId = c.Int(nullable: false, identity: true),
                        PersonId = c.Int(nullable: false),
                        LevelId = c.Int(nullable: false),
                        SemesterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudentEnrollmentId);
            
            CreateTable(
                "dbo.Semesters",
                c => new
                    {
                        SemesterId = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Season = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SemesterId);
            
            AddColumn("dbo.Classes", "SemesterId", c => c.Int(nullable: false));
            CreateIndex("dbo.StudentEnrollments", "SemesterId");
            CreateIndex("dbo.StudentEnrollments", "LevelId");
            CreateIndex("dbo.StudentEnrollments", "PersonId");
            CreateIndex("dbo.Classes", "SemesterId");
            AddForeignKey("dbo.StudentEnrollments", "SemesterId", "dbo.Semesters", "SemesterId", cascadeDelete: true);
            AddForeignKey("dbo.StudentEnrollments", "PersonId", "dbo.People", "PersonId", cascadeDelete: true);
            AddForeignKey("dbo.StudentEnrollments", "LevelId", "dbo.Levels", "LevelId", cascadeDelete: true);
            AddForeignKey("dbo.Classes", "SemesterId", "dbo.Semesters", "SemesterId", cascadeDelete: true);
        }
    }
}
