namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_IsPhotoLoaded_To_Person : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "IsPhotoLoaded", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "IsPhotoLoaded");
        }
    }
}
