namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Level_To_ClassTemplate : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ClassTemplates", "LevelId");
            AddForeignKey("dbo.ClassTemplates", "LevelId", "dbo.Levels", "LevelId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassTemplates", "LevelId", "dbo.Levels");
            DropIndex("dbo.ClassTemplates", new[] { "LevelId" });
        }
    }
}
