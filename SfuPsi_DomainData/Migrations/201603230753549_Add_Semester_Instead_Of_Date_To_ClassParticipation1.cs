namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Semester_Instead_Of_Date_To_ClassParticipation1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Semesters", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Semesters", "Title", c => c.String());
        }
    }
}
