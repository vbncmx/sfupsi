using System.Data.Entity.Migrations;

namespace SfuPsi_DomainData.Migrations
{
    public class Configuration : DbMigrationsConfiguration<SfuPsiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SfuPsiContext context)
        {
            //  This method will be called after migrating to the latest version.
            context.CheckDefaultRoles();
        }
    }
}