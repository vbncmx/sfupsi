namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_SemesterTitle_From_Semester : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Semesters", "SemesterTitle");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Semesters", "SemesterTitle", c => c.String());
        }
    }
}
