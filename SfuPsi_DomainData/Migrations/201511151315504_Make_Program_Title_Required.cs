namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Make_Program_Title_Required : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Programs", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Programs", "Title", c => c.String());
        }
    }
}
