namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Languages_To_Person : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "Languages", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "Languages");
        }
    }
}
