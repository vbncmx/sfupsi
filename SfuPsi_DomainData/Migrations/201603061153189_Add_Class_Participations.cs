namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Class_Participations : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClassParticipations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassTemplateId = c.Int(nullable: false),
                        PersonId = c.Int(nullable: false),
                        Date = c.DateTime(),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassTemplates", t => t.ClassTemplateId, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.ClassTemplateId)
                .Index(t => t.PersonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassParticipations", "PersonId", "dbo.People");
            DropForeignKey("dbo.ClassParticipations", "ClassTemplateId", "dbo.ClassTemplates");
            DropIndex("dbo.ClassParticipations", new[] { "PersonId" });
            DropIndex("dbo.ClassParticipations", new[] { "ClassTemplateId" });
            DropTable("dbo.ClassParticipations");
        }
    }
}
