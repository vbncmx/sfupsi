using SfuPsi_DomainClasses;

namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Season_Year_To_Semester : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Semesters", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.Semesters", "Season", c => c.Int(nullable: false));
            AlterColumn("dbo.Semesters", "SemesterTitle", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Semesters", "SemesterTitle", c => c.String(nullable: false));
            DropColumn("dbo.Semesters", "Season");
            DropColumn("dbo.Semesters", "Year");
        }
    }
}
