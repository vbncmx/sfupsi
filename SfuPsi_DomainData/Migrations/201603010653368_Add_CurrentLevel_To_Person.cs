namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_CurrentLevel_To_Person : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "CurrentLevelId", c => c.Int());
            CreateIndex("dbo.People", "CurrentLevelId");
            AddForeignKey("dbo.People", "CurrentLevelId", "dbo.Levels", "LevelId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.People", "CurrentLevelId", "dbo.Levels");
            DropIndex("dbo.People", new[] { "CurrentLevelId" });
            DropColumn("dbo.People", "CurrentLevelId");
        }
    }
}
