namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_PhotoUrl_From_Person : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.People", "PhotoUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.People", "PhotoUrl", c => c.String());
        }
    }
}
