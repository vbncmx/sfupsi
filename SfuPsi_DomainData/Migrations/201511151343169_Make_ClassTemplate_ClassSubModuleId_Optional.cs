namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Make_ClassTemplate_ClassSubModuleId_Optional : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ClassTemplates", "ClassSubModuleId", c => c.Int());
            CreateIndex("dbo.ClassTemplates", "ClassSubModuleId");
            AddForeignKey("dbo.ClassTemplates", "ClassSubModuleId", "dbo.SubModules", "SubModuleId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassTemplates", "ClassSubModuleId", "dbo.SubModules");
            DropIndex("dbo.ClassTemplates", new[] { "ClassSubModuleId" });
            AlterColumn("dbo.ClassTemplates", "ClassSubModuleId", c => c.Int(nullable: false));
        }
    }
}
