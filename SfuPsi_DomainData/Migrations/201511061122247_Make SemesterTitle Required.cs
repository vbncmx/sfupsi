namespace SfuPsi_DomainData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeSemesterTitleRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Semesters", "SemesterTitle", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Semesters", "SemesterTitle", c => c.String());
        }
    }
}
