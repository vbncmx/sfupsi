﻿using System;
using SfuPsi_DomainClasses;

namespace SfuPsi_DomainData
{
    public static class ContextExtensions
    {
        public static SfuPsiContext CheckDefaultRoles(this SfuPsiContext context)
        {
            // check student role
            var studentRole = context.Roles.Find(Role.StudentRoleId);
            if (studentRole != null)
            {
                studentRole.RoleTitle = Role.StudentRoleTitle;
                context.SaveChanges();
            }
            else
            {
                context.InsertRoleUsing_IDENTITY_INSERT(Role.StudentRoleId, Role.StudentRoleTitle);
            }

            // check professor role
            var professorRole = context.Roles.Find(Role.ProfessorRoleId);
            if (professorRole != null)
            {
                professorRole.RoleTitle = Role.ProfessorRoleTitle;
                context.SaveChanges();
            }
            else
            {
                context.InsertRoleUsing_IDENTITY_INSERT(Role.ProfessorRoleId, Role.ProfessorRoleTitle);
            }

            return context;
        }

        private static void InsertRoleUsing_IDENTITY_INSERT(this SfuPsiContext context, int roleId, string roleTitle)
        {
            var query = $"SET IDENTITY_INSERT Roles ON;{Environment.NewLine}";
            query += $"INSERT INTO Roles ({nameof(Role.RoleId)}, {nameof(Role.RoleTitle)}) VALUES{Environment.NewLine}";
            query += $"({roleId}, '{roleTitle}');{Environment.NewLine}";
            query += "SET IDENTITY_INSERT Roles OFF;";
            context.Database.ExecuteSqlCommand(query);
        }
    }
}